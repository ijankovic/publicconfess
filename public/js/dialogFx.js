/**
 * dialogFx.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
;( function( window ) {
	
	'use strict';

	var scroll = 0;

	var support = { animations : Modernizr.cssanimations },
		animEndEventNames = { 'WebkitAnimation' : 'webkitAnimationEnd', 'OAnimation' : 'oAnimationEnd', 'msAnimation' : 'MSAnimationEnd', 'animation' : 'animationend' },
		animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		onEndAnimation = function( el, callback ) {
			var onEndCallbackFn = function( ev ) {
				if( support.animations ) {
					if( ev.target != this ) return;
					this.removeEventListener( animEndEventName, onEndCallbackFn );
				}
				if( callback && typeof callback === 'function' ) { callback.call(); }
			};
			if( support.animations ) {
				el.addEventListener( animEndEventName, onEndCallbackFn );
			}
			else {
				onEndCallbackFn();
			}
		};

	function extend( a, b ) {
		for( var key in b ) { 
			if( b.hasOwnProperty( key ) ) {
				a[key] = b[key];
			}
		}
		return a;
	}

	function DialogFx( el, options ) {
		this.el = el;
		this.options = extend( {}, this.options );
		extend( this.options, options );
		this.ctrlClose1 = this.el.querySelector( '[data-dialog-close1]' );
		this.ctrlClose2 = this.el.querySelector( '[data-dialog-close2]' );
		this.ctrlClose3 = this.el.querySelector( '[data-dialog-close3]' );
		this.ctrlClose4 = this.el.querySelector( '[data-dialog-close4]' );
		this.isOpen = false;
		this._initEvents();
	}

	DialogFx.prototype.options = {
		// callbacks
		onOpenDialog : function() {

			// Send "Confess - Open Modal" event to Google Analytics.
			ga('send', 'event', 'Confess', 'Open Modal');

			setTimeout(function() { $('.dialog__content textarea').focus(); }, 800);
			return false;
		},
		onCloseDialog : function() {

			// Since modal closing is CSS transformation, we cannot use JavaScript callbacks.
			// That's why we have to set timeout of 400 ms to wait for the CSS transformation to finish.
			setTimeout(function(){ resetConfessModalToInitial(); }, 400);
			return false;
		}
	}

	DialogFx.prototype._initEvents = function() {
		var self = this;

		// close action
		this.ctrlClose1.addEventListener( 'click', this.toggle.bind(this) );
		this.ctrlClose2.addEventListener( 'click', this.toggle.bind(this) );
		this.ctrlClose3.addEventListener( 'click', this.toggle.bind(this) );
		this.ctrlClose4.addEventListener( 'click', this.toggle.bind(this) );

		// esc key closes dialog
		document.addEventListener( 'keydown', function( ev ) {
			var keyCode = ev.keyCode || ev.which;
			if( keyCode === 27 && self.isOpen ) {
				self.toggle();
			}
		} );

		this.el.querySelector( '.dialog__overlay' ).addEventListener( 'click', this.toggle.bind(this) );
	}

	DialogFx.prototype.toggle = function() {

		var self = this;

		var mainContainter = $('body > .main');

		if( this.isOpen ) {

			mainContainter.removeAttr( 'style' );

			if ( !isItMobileScreen() ) {
				mainContainter.removeClass('blur');
			}

			$(document).scrollTop(scroll);

			classie.remove( this.el, 'dialog--open' );
			classie.add( self.el, 'dialog--close' );
			
			onEndAnimation( this.el.querySelector( '.dialog__content' ), function() {
				classie.remove( self.el, 'dialog--close' );
			} );

			// callback on close
			this.options.onCloseDialog( this );
		}
		else {

			scroll = $(document).scrollTop();
			mainContainter.css('position', 'fixed').css('top', -scroll + 'px');

			if ( !isItMobileScreen() ) {
				mainContainter.addClass('blur');
			}

			classie.add( this.el, 'dialog--open' );

			// callback on open
			this.options.onOpenDialog( this );
		}
		this.isOpen = !this.isOpen;
	};

	// add to global namespace
	window.DialogFx = DialogFx;

})( window );