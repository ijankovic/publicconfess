
// var url = "http://localhost:8080";
// var url = "http://publicconfess.herokuapp.com";
var url = "http://www.publicconfess.com";

var api_url = url + "/api";

// ----------------------------------------------------------------


function getConfessionId(element) {
	return $(element).parents('div[id^="confession-"]').attr('id');
}

function getCleanConfessionId(element) {
	return getConfessionId(element).replace('confession-', '');
}

// Function used to extract url parameters
function readParams(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results==null){
		return null;
	}
	else{
		return results[1] || 0;
	}
}

// Check if screen size is for mobile devices.
function isItMobileScreen() {
	if ($(window).width() < 450) {
		return true;
	} else {
		return false;
	}
}

// stickyNav is only used for tablet screen or more.
function stickyNav() {
	var scrollTop = $(window).scrollTop();

	if (scrollTop > 100) { 
		$('.header_small').addClass('sticky');
		//$('.header_small').animate({background: 'rgba(51, 51, 255, 1.0)'});
		$('.logo-mini').fadeIn( "slow" );
		$('.container').css('top', '50px');
		$('footer').css('top', '50px');
		
	} else {
		// Check if modal is opened. If it is not opened then remove .sticky from header_small.
		if ($('.main').css('position') != 'fixed') {
			$('.header_small').removeClass('sticky').removeAttr('style');

			$('.logo-mini').fadeOut( "slow" );
			$('.container').css('top', '0px');
			$('footer').css('top', '0px');
		}
	}
}


function updateCharCount() {
    // 1000 is the max message length
    var remaining = 1000 - $('#confession_text').val().length;
    $('.char-count').text(remaining);

    if (remaining <= 10) {
		$('.char-count').css('color', '#ff3131').css('font-weight', 'bold');
    } else {
    	$('.char-count').removeAttr('style');
    }
}


function AutoGrowTextArea(textField)
{
	// We subtract 12 because there is padding in textarea (6px on the top and 6px on the bottom)
	$(textField).css({'height': 'auto', 'overflow-y':'hidden'}).height(textField.scrollHeight - 12);
}


function afterSubmitConfessionSuccess(confession_id) {

	var data = { "confession_id": confession_id}
	var template = '';

	template += '<span class="icon-check2"></span>' +
				'<h2>Confession submitted! </h2>' +
				'<h3>Your confession id is <span class="confession-id">{{ confession_id }}</span>.</h3>' +
				'<p>It will show up in "Be Admin" section shortly. You can view your confession here:</p>' +
				'<a href="/confessions/{{ confession_id }}" class="confession-link">publicconfess.com/confessions/{{ confession_id }}</a>';

	return Mustache.render(template, data);
}

function afterSubmitConfessionFailContent() {
	var output = '';
	output += '<span class="icon-cross2"></span>';
	output += '<h2>Oops! Confession NOT submitted!</h2>';
	output += '<h3>Something is wrong with your confession.</h3>';
	output += '<p>Please do not write swear words, emails or links in your confession.</p>';

	return output;
}

function afterSubmitConfessionFailServer() {
	var output = '';
	output += '<span class="icon-cross2"></span>';
	output += '<h2>Oops! Confession could not be submitted!</h2>';
	output += '<h3>Something went wrong on our server.</h3>';
	output += '<p>We are trying to fix it. Try again later.</p>';

	return output;
}

function resetConfessModalToInitial() {
	$('.confess-modal .after').hide();
	$('.confess-modal .before').show();

	// Remove height that was added through jQuery animation.
	$('.confess-modal .dialog__content').removeAttr( 'style' );
	$('#confession_text').val('');	// empty textfield
	updateCharCount();	// reset character count
}

function convertToRelativeTimeConfession() {

	$('.confession_time').each(function(index, value) {
		var timestamp = $(this).attr('timestamp');
		var relative_time = moment(timestamp).fromNow();
		$(this).text(relative_time);
	});
}

function convertToRelativeTimeComment(confession_id) {

	$('#' + confession_id + ' .comment_time').each(function(index, value) {
		var timestamp = $(this).attr('timestamp');
		var relative_time = moment(timestamp).fromNow();
		$(this).text(relative_time);
	});
}

function renderComments(data, hidden) {

	var template = '';

	if (hidden) {
		template += '{{ #. }}<div id="comment-{{ _id }}" class="comment_wrap" style="display:none">';
	} else {
		template += '{{ #. }}<div id="comment-{{ _id }}" class="comment_wrap">';
	}
	
	template += '<div class="comment">' +
				'<p>{{ content }}</p>' +
				'<div class="comment_footer row">' +
					'<div class="comment_controls">' +
						'<div class="comment_action like_comment">' +
							'<span class="desktop-screen icon icon-thumbs-o-up"></span>' +
							'<span class="label">{{ likes }}</span>' +
						'</div>' +
						'<div class="comment_action dislike_comment">' +
							'<span class="desktop-screen icon icon-thumbs-o-down"></span>' +
							'<span class="label">{{{ dislikes }}}</span>' +
						'</div>' +
					'</div>' +
					'<div class="comment_info">' +
						'<span class="author">{{ author }}</span>' +
						'<span class="separator"> · </span>' +
						'<span class="comment_time" timestamp="{{ timestamp }}"></span>' +
					'</div>' +
				'</div>' + 
			'</div>' + 
		'</div>{{ /. }}';


	return Mustache.render(template, data);
}

function renderCommentError(data) {

	var template = '';
	template += '<div id="comment-info" class="comment_wrap">';
	template += '<div class="comment">' +
					'<p>{{ message }}</p>' +
				'</div>';

	return Mustache.render(template, data);
}

function loadComments(confession_id, callback) {

	// Remove prefix used in id of html element.
	var confession_id_clean = confession_id.replace('confession-', '');

	var total_comments = parseInt($('#' + confession_id + ' .comments_action .label').text());
	var current_comments = $('#' + confession_id + ' .comment').length;

	if (total_comments == 0) {

		$('#' + confession_id + ' .no_comments_message').show();
		callback();

	} else if (total_comments > 0) {

		$.get( api_url + "/confessions/" + confession_id_clean + "/comments", {limit: 10, offset: current_comments, sortby:"time", sortorder:"desc"}, function( data ) {

			var rendered = renderComments(data, false);
			$('#' + confession_id + ' .comments').append(rendered);

			$('#' + confession_id + ' .comments_container_footer').show();

			current_comments = $('#' + confession_id + ' .comment').length;

			if (total_comments - current_comments > 0) {
				$('#' + confession_id + ' .view_more_comments').show();
			} else {
				$('#' + confession_id + ' .view_more_comments').hide();
			}

			callback();
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			$('#' + confession_id + ' .comments').html('<p class="no_comments_message">There was a problem getting comments.</p>');
			callback();
		});
	}
}


function displayNewComment(confession_id, data) {

	var comment_textarea = $('#' + confession_id + ' .write_comment_container textarea');
	var author_field = $('#' + confession_id + ' .write_comment_container input');

	// Animation workaround to fadeout comment textarea and author input field when comment has been
	// successfully posted. There is a delay of 500ms because there is css transition of 500ms on color
	// of textareas and input fields.
	comment_textarea.css("color", "rgba(255, 255, 255, 0)");
	author_field.css("color", "rgba(255, 255, 255, 0)");
	setTimeout(function(){
		comment_textarea.val('');
		author_field.val('');
		comment_textarea.removeAttr('style');
		author_field.removeAttr('style');
	}, 500);

	// Change label for number of comments to count the new comment.
	var num_of_comments = parseInt($('#' + confession_id + ' .comments_action .label').text());
	$('#' + confession_id + ' .comments_action .label').text(num_of_comments + 1);

	// If there were no comments before, it means that now first comment is being is posted and 
	// we need to hide no comments message.
	if (num_of_comments == 0) {
		$('#' + confession_id + ' .no_comments_message').hide();
	}

	// rendered will contain comment with style="display:none". We will reveal it by using animation.
	var rendered = renderComments(data, true);
	$('#' + confession_id + ' .comments').prepend(rendered);
	convertToRelativeTimeComment(confession_id);

	// Select newly added comment and reveal it.
	$('#comment-' + data._id).slideDown(300);
}


function displayCommentMessage(confession_id, message) {
	var write_comment_container = $('#' + confession_id + ' .write_comment_container');

	write_comment_container.fadeOut(600, function() {
		write_comment_container.html('<p class="submit_comment_message">' + message + '</p>');
		write_comment_container.fadeIn(600);
	});

	setTimeout(function(){
		write_comment_container.fadeOut(600);
	}, 3000);
}


function checkIfCommentTooFast() {
	var last_comment_time = localStorage.getItem("last_comment_time");

	if (last_comment_time && moment().utc().diff(last_comment_time, 'seconds') <= 30) {
		return true;
	} else {
		return false;
	}
}


function labelActiveConfessions() {

	for (var i = 0; i < localStorage.length; i++) {
		var id = localStorage.key(i);

		if (id.indexOf('confession') != -1) {
			var action = localStorage.getItem(id);

			switch (action) {
				case 'approve':
					$('#' + id + ' .approve_action').addClass('approve_action_active');
					break;
				case 'condemn':
					$('#' + id + ' .condemn_action').addClass('condemn_action_active');
					break;
			}
		}
	}
}


function labelActiveComments() {
	for (var i = 0; i < localStorage.length; i++) {
		var id = localStorage.key(i);

		if (id.indexOf('comment') != -1) {
			var action = localStorage.getItem(id);

			switch (action) {
				case 'like':
					$('#' + id + ' .like_comment').addClass('comment_action_active');
					break;
				case 'dislike':
					$('#' + id + ' .dislike_comment').addClass('comment_action_active');
					break;
			}
		}
	}
}


function finishAnimation(confession_content, footer_height) {

	// footer_height is optional parameter. If no value is passed, footer_height is set to 0.
	if ( footer_height === undefined ) {
		footer_height = 0; // default value
	}

	var pcw = $('.pending_confession_wrap');
	var h = pcw.height(); // current height
	//pcw.css("height", h + 'px');
	$('.pending_confession_wrap p').text(confession_content);
	pcw.css("height", "auto");

	// footer_height is used only when there are no more pending confessions to accomodate
	// removal of "Yes" and "No" buttons.
	var h2 = pcw.height() - footer_height; // new height
	pcw.height(h); // set old height to run animation
	pcw.animate({ height: h2 }, 300, function() {
		// After animation is over set height to auto. This is if user resizes browser window,
		// height cannot be fixed, since contents could go out of the container.
		pcw.css("height", "auto");
		if (footer_height) {
			$('.pending_confession_footer').remove();
		}
		$('.confession_container').animate({ opacity: 1 }, 300);
	});
}


function renderNextPendingConfession() {

	// Fade out the previous confession
	$('.confession_container').animate({ opacity: 0 }, 300, function() {
		$('.confession_container').trigger("animationComplete");
	});

	getNextPendingConfession(function(confession) {

		var id_to_append;
		var text_to_append;
		var footer_height = 0;

		// Confession is undefined when there are no more pending confessions.
		if ( confession === undefined ) {
			id_to_append = "";
			text_to_append = "No more pending confessions.";
			footer_height = $('.pending_confession_footer').outerHeight(true);
		} else {
			id_to_append = confession._id;
			text_to_append = confession.content;
		}

		// If animation is still running
		if ( $('.confession_container').is(':animated') ) {
			// Data was received during animation. Wait until animation is complete
			// and then call finishAnimation function to do the rest.
			$('.confession_container').one("animationComplete", function() {
				finishAnimation(text_to_append, footer_height);
			});
		// If animation is complete
		} else {
			// Data was received after animation. Call finishAnimation function immediately.
			finishAnimation(text_to_append, footer_height);
		}

		// We store pending confession id inside id attribute of pending_confession_wrap container.
		$('.pending_confession_wrap').attr("id", id_to_append);
	});

}


function renderFirstPendingConfession() {

	getNextPendingConfession(function(confession) {

		var id_to_append;
		var text_to_append;

		// Confession is undefined when there are no more pending confessions.
		if ( confession === undefined ) {
			id_to_append = "";
			text_to_append = "No more pending confessions.";
		} else {
			id_to_append = confession._id;
			text_to_append = confession.content;
			$('.pending_confession_footer').show();
		}

		$('.pending_confession_wrap p').text(text_to_append);

		// We store pending confession id inside id attribute of pending_confession_wrap container.
		$('.pending_confession_wrap').attr("id", id_to_append);
	});
}


function getNextPendingConfession(callback) {

	var lastModeratedId = (localStorage.getItem("lastModeratedId")) ? localStorage.getItem("lastModeratedId") : 0;
	var parameters = { 'lastModeratedId': lastModeratedId };

	$.ajax({
		url: api_url + "/pendingconfessions/moderate",
		data: parameters,
		type:'GET',
		success: function(data) {

			// data is array containing one confession.
			callback(data[0]);
		}
	});
}


function getFirstWords(str, num) {
	// Strip punctuation signs.
	str = str.replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/g,"").replace(/\s{2,}/g," ");
	// Join words and add "..." at the end.
	return str.split(/\s+/).slice(0, num).join(" ") + "...";
}


function getFirstWordsFacebook(str) {
	
	// Strip punctuation signs.
	str = str.replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/g,"").replace(/\s{2,}/g," ");
    
    // If confession has more than 5 words
    if (str.split(/\s+/).length > 5) {
    	// Take only first 5 words and append "..." at the end.
    	return str.split(/\s+/).slice(0, 5).join(" ") + "...";
    } else {
    	// Return whole confession if confession contains 5 words or less.
    	return str;
    }
}

function getFirstWordsTwitter(str) {

	var confession_text = '';

	// If confession has less than 118 characters, output whole confession.
	// 118 characters for confession + 22 characters for url = 140 characters with is the limit.
	if (str.length <= 118) {
		confession_text = str;
	}
	// Since confession is longer than 118 characters, add word by word until you reach 118 character limit.
	else {
		var word_count = 1;
		while(getFirstWords(str, word_count).length < 118) {
			confession_text = getFirstWords(str, word_count);
			word_count += 1;
		}
	}

	return confession_text;
}

function facebookShare(confession_id, content) {

	//content = "Prije pet godina, niko nije bio sretniji od mene kada sam dobila stipendiju i otišla u Ameriku da studiram, što mi je oduvijek bila želja. Bila sam jedan od najboljih studenata, fakultet mi je super išao, ali kada je došlo vrijeme da se nakon diplomiranja zaposlim, tamo nisam mogla dobiti posao u toj struci, jer sam stranac i nisam imala njihovo državljanstvo, a ovdje moje obrazovanje nije priznato. Danas zarađujem za život radeći u butiku i pitam se da li mi je bila životna greška što sam se odlučila za odlazak i školovanje u inostranstvu. Iskren da budem, ne znam da li je Kanada ekonomski bolja kao sto se pise. Svaka drzava ima svoje probleme i neverujem da je Kanada izuzetak. Ipak, moram se sloziti da situacija u Srbiji nije kakva bi mogla da bude.";

	var url = 'https://www.facebook.com/dialog/feed?' +
				'app_id=1385317045129604' +
				'&display=popup' +
				'&name=' + encodeURI(getFirstWordsFacebook(content)) +
				'&description=' + encodeURI(content) +
				'&caption=publicconfess.com' +
				'&link=' + encodeURI('http://publicconfess.com/confessions/') + confession_id +
				'&picture=' + encodeURI("http://publicconfess.com/img/facebook_share_banner.png") +
				'&redirect_uri=http://publicconfess.com/popupClose.html?confession_id=' + confession_id;

	var width  = 626,
		height = 436,
		left   = ($(window).width()  - width)  / 2,
		top    = ($(window).height() - height) / 2,
		//url    = this.href,
		opts   = 'status=0' +
				',toolbar=0' +
				',width='  + width  +
				',height=' + height +
				',top='    + top    +
				',left='   + left;

	window.open(url, 'feedDialog', opts); 
}


function facebookShareCallback(confession_id) {

	// Send API request to increase facebook share count by 1.
	$.ajax( api_url + "/confessions/" + confession_id, {
		type: 'PUT',
		data: { action: 'fb_like' },
		success: function(data) {

			// Send "Confession - Facebook Share - Confession Id" event to Google Analytics.
			window.opener.ga('send', 'event', 'Confession', 'Facebook Share', 'Confession: ' + confession_id);

			// Get current number of facebook shares from parent window.
			var current_shares = parseInt(window.opener.$("#confession-" + confession_id + " .facebook_action .label").text());
			window.opener.$("#confession-" + confession_id + " .facebook_action .label").text(current_shares + 1);

			window.close();
		},
		error: function() {
			window.close();
		}
	});
}


function twitterShare(confession_id, content) {

	//content = "Prije pet godina, niko nije bio sretniji od mene kada sam dobila stipendiju i otišla u Ameriku da studiram, što mi je oduvijek bila želja. Bila sam jedan od najboljih studenata, fakultet mi je super išao, ali kada je došlo vrijeme da se nakon diplomiranja zaposlim, tamo nisam mogla dobiti posao u toj struci, jer sam stranac i nisam imala njihovo državljanstvo, a ovdje moje obrazovanje nije priznato. Danas zarađujem za život radeći u butiku i pitam se da li mi je bila životna greška što sam se odlučila za odlazak i školovanje u inostranstvu. Iskren da budem, ne znam da li je Kanada ekonomski bolja kao sto se pise. Svaka drzava ima svoje probleme i neverujem da je Kanada izuzetak. Ipak, moram se sloziti da situacija u Srbiji nije kakva bi mogla da bude.";

	var url_to_share = encodeURI('http://publicconfess.com/confessions/') + confession_id;

	var twitter_url = 'http://twitter.com/intent/tweet?' + 
						'url=' + url_to_share +
						'&text=' + encodeURI(getFirstWordsTwitter(content));

	var width  = 575,
		height = 400,
		left   = ($(window).width()  - width)  / 2,
		top    = ($(window).height() - height) / 2,
		//url    = this.href,
		opts   = 'status=1' +
				',width='  + width  +
				',height=' + height +
				',top='    + top    +
				',left='   + left;

    // Callback
	$(window).bind("message", function(event) {
		event = event.originalEvent;
		if (event.source == share_window && event.data != "__ready__") {

				// Send API request to increase tweet count by 1.
				$.ajax( api_url + "/confessions/" + confession_id, {
					type: 'PUT',
					data: { action: 'tweet' },
					success: function(data) {

						// Send "Confession - Twitter Share - Confession Id" event to Google Analytics.
						ga('send', 'event', 'Confession', 'Twitter Share', 'Confession: ' + confession_id);

						// Update Twitter share count.
						var current_shares = parseInt($("#confession-" + confession_id + " .twitter_action .label").text());
						$("#confession-" + confession_id + " .twitter_action .label").text(current_shares + 1);
					}
				});
		}
	});

	// Open window
	var share_window = window.open(twitter_url, 'Twitter Share', opts);
}


function isConfessionValid(content) {

	var error = '';

	if (content.length < 20) {
		error = 'Confession must be at least 20 characters long.';
	}

	return error;
}



// ----------------------------------------------------------------------------------------------------------



$(document).ready(function() {

	// Convert timestamps to relative time
	// ----------------------------------------------------------
	convertToRelativeTimeConfession();
	labelActiveConfessions();



	// Used to control fixed header based on the scroll position.
	// ----------------------------------------------------------
	$(window).scroll(function() {

		var didScroll = true;

		//setInterval(function() {

			// Check if it is a tablet screen or more.
			if ($(window).width() >= 768) {
				if (didScroll) {
					stickyNav();
					didScroll = false;
				}
			}
		//}, 50);
	});


	// Called when 'Admit' button is clicked inside confess modal.
	// It will display follow up view inside the same modal without closing it.
	// ––––––––––––––––––––––––––––––––––––––––––––––––––----------------------
	$('.admit').click( function () {

		var confession_content = $('#confession_text').val();
		var confessionContentError = isConfessionValid(confession_content);

		if (confessionContentError == '') {

			// Replace double space with single space.
			confession_content = confession_content.replace(/  /g, " ");

			$.post( api_url + "/pendingconfessions", { content: confession_content }, function( data ) {

				// Send "Confess - Admit - Success" event to Google Analytics.
				ga('send', 'event', 'Confess', 'Admit', 'Success');
			
				var confession_id = data._id;
				
				$('.confess-modal .after .title').html("Confession submitted successfully!");

				// Insert HTML into .confess-modal .after .main
				$('#afterContent').html(afterSubmitConfessionSuccess(confession_id));

				$('.confess-modal .before').fadeOut(400, function() {
					// 555px is height of confess modal after submit.
					$('.dialog__content').animate({height: '430px'}, 400, function() {
							$('.confess-modal .after').fadeIn(400);	
					});

				});
			})
			.fail(function(jqXHR, textStatus, errorThrown) {

				// Send "Confess - Admit - Server Fail" event to Google Analytics.
				ga('send', 'event', 'Confess', 'Admit', 'Server Fail');

				if (jqXHR.status == 400) {
					// alert(jqXHR.responseText);
					// afterMessage = JSON.parse(jqXHR.responseText);
					// alert(afterMessage.error);

					// Insert HTML into .confess-modal .after .main
					$('#afterContent').html(afterSubmitConfessionFailContent());

				} else if (jqXHR.status == 500) {
					// Insert HTML into .confess-modal .after .main
					$('#afterContent').html(afterSubmitConfessionFailServer());
				} else {
					$('#afterContent').html(afterSubmitConfessionFailServer());
				}

				$('.confess-modal .after .title').html("Confession not submitted!");

				$('.confess-modal .before').fadeOut(400, function() {
					// 363px is height of confess modal after submit.
					$('.dialog__content').animate({height: '363px'}, 400, function() {
						$('.confess-modal .after').fadeIn(400);		
					});
				});
			});

		} else {

			// Send "Confess - Admit - Content Error" event to Google Analytics.
			ga('send', 'event', 'Confess', 'Admit', 'Content Error');

			$('button.admit').attr('disabled','disabled');
			$('.dialog__content .main form').fadeOut(400, function() {
				$('.dialog__content .main').append('<p class="invalidConfessionNote">' + confessionContentError + '</p>');
				$('.invalidConfessionNote').fadeIn(400, function() {

					// Display Notification for 2000ms and fade back to textarea.
					setTimeout(function() { 
						$('.dialog__content .main .invalidConfessionNote').fadeOut(400, function() {
							$('.dialog__content .main .invalidConfessionNote').remove();
							$('.dialog__content .main form').fadeIn(400, function() {
								$('.dialog__content textarea').focus();
								$('button.admit').removeAttr('disabled');
							});
						});
					}, 2000);
				});
			});
		}

	});


	// Updating char counter when writing confession.
	// ––––––––––––––––––––––––––––––––––––––––––––––
	//updateCharCount();
	$('#confession_text').change(updateCharCount);
	$('#confession_text').keyup(updateCharCount);

	// Prevent new line when writing a confession.
	// $('#confession_text').keypress(function(event) {
	// 	if (event.keyCode == 13) {
	// 		event.preventDefault();
	// 	}
	// });


	// Control toggle of comments container
	// ––––––––––––––––––––––––––––––––––––––––––––––

	open_comments = {}

	function toggleCommentsContainer(confession_id) {

		var open;

		if (confession_id in open_comments) {
			open = !open_comments[confession_id];
		} else {
			open = true;
		}

		open_comments[confession_id] = open;


		$('#'+ confession_id + ' .comments_container').slideToggle( { 

			start: function() {

				if (!open) {
					$('#'+ confession_id + ' .comments_container').css('border-color', 'rgba(182, 187, 190, 0.0)');
				}

			},

			done: function() {

				// Animating border
				if (open) {
					$('#'+ confession_id + ' .comments_container').css('border-color', 'rgba(182, 187, 190, 0.15)');
				}
			}
		});

		// Keep comments icon glowing when comments container is open.
		if (open) {
			$('#' + confession_id + ' .comments_action').addClass('comments_action_active');
		} else {
			$('#' + confession_id + ' .comments_action').removeClass('comments_action_active');
		}

	}


	// Opening confession's comment container
	// ––––––––––––––––––––––––––––––––––––––––––––––
	var loaded_comments = [];

	$('.comments_action').click(function() {

		// On single confession page, we disable comments container toggle.
		if (!$(this).hasClass("action_disabled")) {
			
			// Find confession id by searching for parent div and extracting id attribute value.
			var confession_id = $(this).parents('div[id^="confession-"]').attr('id');
			var confession_id_clean = confession_id.replace('confession-', '');

			// If comments have not been loaded already for this confession, load them now.
			if (loaded_comments.indexOf(confession_id) == -1) {
				loadComments(confession_id, function() {
					convertToRelativeTimeComment(confession_id);
					labelActiveComments();
					toggleCommentsContainer(confession_id);
				});

				loaded_comments.push(confession_id);	// Add confession id to list for future checks.



				// Send "Confession - Open Comments - Confession Id" event to Google Analytics.
				// We are interested in open comments event analytics only when comments container
				// has been open for the first time.
				ga('send', 'event', 'Comment', 'Open Comments', 'Confession: ' + confession_id_clean);

			} else {

				toggleCommentsContainer(confession_id);
			}
		}
	});



	// Click on approve or condemn action
	// ––––––––––––––––––––––––––––––––––––––––––––––
	$('.approve_action').click(function() {

		// Find confession id by searching for parent div and extracting id attribute value.
		var confession_id = $(this).parents('div[id^="confession-"]').attr('id');

		// Remove prefix used in id of html element.
		var confession_id_clean = confession_id.replace('confession-', '');

		// Check if approve or condemn has already been clicked
		if (localStorage.getItem(confession_id)) {
			return false;
		} else {
			// Keep icon glowing when clicked.
			$('#' + confession_id + ' .approve_action').addClass('approve_action_active');

			// Add confession_id and action to HTML 5 local storage.
			// This is how we prevent users from approving the same confession more than once.
			localStorage.setItem(confession_id, 'approve');

			$.ajax( api_url + "/confessions/" + confession_id_clean, {
				type: 'PUT',
				data: { action: 'approve' },
				success: function(data) {

					var current_approve = parseInt($('#' + confession_id + ' .approve_action .label').text());

					$('#' + confession_id + ' .approve_action .label').text(current_approve + 1);

					// Send "Confession - Approve - Confession Id" event to Google Analytics.
					ga('send', 'event', 'Confession', 'Approve', 'Confession: ' + confession_id_clean);
				},
				error: function() {
					// ERROR
				}
			});
		}
	});


	$('.condemn_action').click(function() {

		// Find confession id by searching for parent div and extracting id attribute value.
		var confession_id = $(this).parents('div[id^="confession-"]').attr('id');

		// Remove prefix used in id of html element.
		var confession_id_clean = confession_id.replace('confession-', '');

		// Check if approve or condemn has already been clicked
		if (localStorage.getItem(confession_id)) {
			return false;
		} else {
			// Keep icon glowing when clicked.
			$('#' + confession_id + ' .condemn_action').addClass('condemn_action_active');

			// Add confession_id and action to HTML 5 local storage.
			// This is how we prevent users from approving the same confession more than once.
			localStorage.setItem(confession_id, 'condemn');

			$.ajax( api_url + "/confessions/" + confession_id_clean, {
				type: 'PUT',
				data: { action: 'condemn' },
				success: function(data) {

					var current_condemn = parseInt($('#' + confession_id + ' .condemn_action .label').text());
					
					$('#' + confession_id + ' .condemn_action .label').text(current_condemn + 1);

					// Send "Confession - Condemn - Confession Id" event to Google Analytics.
					ga('send', 'event', 'Confession', 'Condemn', 'Confession: ' + confession_id_clean);
				},
				error: function() {
					// ERROR
				}
			});
		}
	});


	// Click on like or dislike comment action
	// ––––––––––––––––––––––––––––––––––––––––––––––


	$('.write_comment_container textarea').on('input', function() {
		AutoGrowTextArea(this);
	});


	var expended_textareas = [];

	$('.write_comment_container textarea').focus(function() {

		// Find confession id by searching for parent div and extracting id attribute value.
		var confession_id = $(this).parents('div[id^="confession-"]').attr('id');

		// Check if .write_comment_container_expand div has already been expended for this confession.
		if (expended_textareas.indexOf(confession_id) > -1) {
			// If it is, don't do anything.
			return false;
		} else {
			// If it isn't add confession id to list for future checks.
			expended_textareas.push(confession_id);

			$('#' + confession_id + ' .write_comment_container_expand').css('opacity', 0)
																	.slideDown('fast')
																	.animate(
																		{ opacity: 1 },
																		{ queue: false, duration: 600 }
																	);
		}
	});



	$('.view_more_comments').click(function() {

		// Find confession id by searching for parent div and extracting id attribute value.
		var confession_id = $(this).parents('div[id^="confession-"]').attr('id');
		var confession_id_clean = confession_id.replace('confession-', '');

		loadComments(confession_id, function() {
			convertToRelativeTimeComment(confession_id);
			labelActiveComments();
		});

		// Send "Comment - View More - Confession Id" event to Google Analytics.
		ga('send', 'event', 'Comment', 'View More', 'Confession: ' + confession_id_clean);

	});



	$('.comments_container_footer_action.write_comment').click(function() {

		// Find confession id by searching for parent div and extracting id attribute value.
		var confession_id = $(this).parents('div[id^="confession-"]').attr('id');

		$('html, body').animate({
									scrollTop: $('#' + confession_id + ' .write_comment_container textarea').offset().top - 120
								}, 800, function() {
									setTimeout(function() { $('#' + confession_id + ' .write_comment_container textarea').focus(); }, 200);
									
								});
									
	});



	$('.comments_container_footer_action.back_to_confession').click(function() {

		// Find confession id by searching for parent div and extracting id attribute value.
		var confession_id = $(this).parents('div[id^="confession-"]').attr('id');

		$('html, body').animate({
									scrollTop: $('#' + confession_id).offset().top - 100
								}, 800);		
	});




	$('.post_comment').click(function() {

		// Find confession id by searching for parent div and extracting id attribute value.
		var confession_id = $(this).parents('div[id^="confession-"]').attr('id');
		// Remove prefix used in id of html element.
		var confession_id_clean = confession_id.replace('confession-', '');

		var comment_textarea = $('#' + confession_id + ' .write_comment_container textarea');
		var author_field = $('#' + confession_id + ' .write_comment_container input');

		var comment_content = comment_textarea.val();
		var comment_author = author_field.val();

		if (comment_content == '') {
			alert('Comment cannot be empty.');
			return;
		}

		if (comment_author == '') {
			comment_author = 'Anonymous';
		}

		$.ajax({
			url: api_url + "/confessions/" + confession_id_clean + "/comments",
			data: { content: comment_content, author: comment_author },
			type:'POST',
			beforeSend: function(xhr) {
				// Show waiting signal animation
				$('#' + confession_id + ' .signal_new_comment').show();

				if (checkIfCommentTooFast()) {
					displayCommentMessage(confession_id, "Take a breath! Do not post comments so fast.");
					xhr.abort();
				} else {
					localStorage.setItem("last_comment_time", moment().utc().toISOString());
				}
			},
			success: function(data) {

				// Send "Comment - Post - Confession Id" event to Google Analytics.
				ga('send', 'event', 'Comment', 'Post', 'Confession: ' + confession_id_clean);

				// Hide waiting signal animation
				$('#' + confession_id + ' .signal_new_comment').hide();

				if (data.message) {

					// data object contains message field. This means that comment is not ready to be published on the site.
					displayCommentMessage(confession_id, data.message);

				} else {

					// data object contains no message field. This means that comment is ok and we can display it right away.
					displayNewComment(confession_id, data);
				}

			},
		});
	});


	$(document).on('click', '.like_comment', function() {

		// Find confession id by searching for parent div and extracting id attribute value.
		var confession_id = $(this).parents('div[id^="confession-"]').attr('id');
		// Remove prefix used in id of html element.
		var confession_id_clean = confession_id.replace('confession-', '');
	
		var comment_id = $(this).parents('div[id^="comment-"]').attr('id');
		var comment_id_clean = comment_id.replace('comment-', '');

		if (localStorage.getItem(comment_id)) {
			return false;
		} else {
			// Keep icon glowing when clicked.
			$('#' + comment_id + ' .like_comment').addClass('comment_action_active');

			// Add comment and action to HTML 5 local storage.
			// This is how we prevent users from liking the same comment more than once.
			localStorage.setItem(comment_id, 'like');

			$.ajax( api_url + "/comments/" + comment_id_clean, {
				type: 'PUT',
				data: { action: 'like' },
				success: function(data) {

					var current_likes = parseInt($('#' + comment_id + ' .like_comment .label').text());
					$('#' + comment_id + ' .like_comment .label').text(current_likes + 1);

					// Send "Comment - Like - Confession Id, Comment Id" event to Google Analytics.
					ga('send', 'event', 'Comment', 'Like', 'Confession: ' + confession_id_clean + ', Comment: ' + comment_id_clean);
				},
				error: function() {
					// ERROR
				}
			});
		}
	});


	$(document).on('click', '.dislike_comment', function() { 

		// Find confession id by searching for parent div and extracting id attribute value.
		var confession_id = $(this).parents('div[id^="confession-"]').attr('id');
		// Remove prefix used in id of html element.
		var confession_id_clean = confession_id.replace('confession-', '');
	
		var comment_id = $(this).parents('div[id^="comment-"]').attr('id');
		var comment_id_clean = comment_id.replace('comment-', '');

		if (localStorage.getItem(comment_id)) {
			return false;
		} else {
			// Keep icon glowing when clicked.
			$('#' + comment_id + ' .dislike_comment').addClass('comment_action_active');

			// Add comment and action to HTML 5 local storage.
			// This is how we prevent users from liking the same comment more than once.
			localStorage.setItem(comment_id, 'dislike');

			$.ajax( api_url + "/comments/" + comment_id_clean, {
				type: 'PUT',
				data: { action: 'dislike' },
				success: function(data) {

					var current_dislikes = parseInt($('#' + comment_id + ' .dislike_comment .label').text());
					$('#' + comment_id + ' .dislike_comment .label').text(current_dislikes + 1);

					// Send "Comment - Dislike - Confession Id, Comment Id" event to Google Analytics.
					ga('send', 'event', 'Comment', 'Dislike', 'Confession: ' + confession_id_clean + ', Comment: ' + comment_id_clean);
				},
				error: function() {
					// ERROR
				}
			});
		}
	});


	// $('.like_comment').click(function() {

	// 	// Find confession id by searching for parent div and extracting id attribute value.
	// 	var comment_id = $(this).parents('div[id^="comment-"]').attr('id');

	// 	// Check if dislke action is already clicked
	// 	if ($('#' + comment_id + ' .dislike_comment').hasClass('comment_action_active')) {
	// 		// Approve action has already been clicked. Don't do anything in that case.
	// 		return false;
	// 	} else {
	// 		// Keep icon glowing when clicked.
	// 		$('#' + comment_id + ' .like_comment').addClass('comment_action_active');
	// 	}

	// });



	// $('.dislike_comment').click(function() {

	// 	// Find confession id by searching for parent div and extracting id attribute value.
	// 	var comment_id = $(this).parents('div[id^="comment-"]').attr('id');

	// 	// Check if dislke action is already clicked
	// 	if ($('#' + comment_id + ' .like_comment').hasClass('comment_action_active')) {
	// 		// Approve action has already been clicked. Don't do anything in that case.
	// 		return false;
	// 	} else {
	// 		// Keep icon glowing when clicked.
	// 		$('#' + comment_id + ' .dislike_comment').addClass('comment_action_active');
	// 	}

	// });


	// Moderate page
	// ––––––––––––––––––––––––––––––––––––––––––––––

	$('button.yes').click(function() {

		// Id of pending confession is store in id attribute of pending_confession_wrap container.
		var confession_id = $('.pending_confession_wrap').attr("id");

		localStorage.setItem("lastModeratedId", confession_id);
		renderNextPendingConfession();

		// Send request REST api to increase 'Yes' vote count by 1
		// The request is done in parallel while next pending confession is rendered. We do not need to wait
		// for the response from the server that voting was successful since either way next pending confession
		// will be displayed.
		$.ajax({
			url: api_url + "/pendingconfessions/" + confession_id,
			data: { action: 'yes'},
			type: 'PUT',
		});

		// Send "Moderate - Yes - Confession Id" event to Google Analytics.
		ga('send', 'event', 'Moderate', 'Yes', 'Confession: ' + confession_id);
	});


	$('button.no').click(function() {

		// Id of pending confession is store in id attribute of pending_confession_wrap container.
		var confession_id = $('.pending_confession_wrap').attr("id");

		localStorage.setItem("lastModeratedId", confession_id);
		renderNextPendingConfession();

		// Send request REST api to increase 'No' vote count by 1
		$.ajax({
			url: api_url + "/pendingconfessions/" + confession_id,
			data: { action: 'no'},
			type: 'PUT',
		});

		// Send "Moderate - No - Confession Id" event to Google Analytics.
		ga('send', 'event', 'Moderate', 'No', 'Confession: ' + confession_id);
	});


	// Contact pages
	// ––––––––––––––––––––––––––––––––––––––––––––––

	$('.send_button').click(function() {

		var form_name = $("input[name='form-name']").val();

		var name = $('.name_field').val();
		var email = $('.email_field').val();
		var message = $('.message_body').val();
		message = message.replace(/(\r\n|\r|\n)/g,"<br/>");

		$.post( api_url + "/" + form_name + "/send", { senderName: name, senderEmail: email, messageBody: message }, function() {

			$(".send_button").fadeOut(600, function() {
				$(".contact_form").append("<p>Email sent successfully!</p>")
			});
			
		});
	});




	// Social Sharing
	// ––––––––––––––––––––––––––––––––––––––––––––––

	$('.facebook_action').click(function() {

		var confession_id_clean = getCleanConfessionId(this);
		var confession_id = "confession-" + confession_id_clean;
		var confession_content = $("#" + confession_id + " .confession p").text();

		facebookShare(confession_id_clean, confession_content);


	});

	$('.twitter_action').click(function() {

		var confession_id_clean = getCleanConfessionId(this);
		var confession_id = "confession-" + confession_id_clean;
		var confession_content = $("#" + confession_id + " .confession p").text();

		twitterShare(confession_id_clean, confession_content);
	});


	// Social Media Links
	// ––––––––––––––––––––––––––––––––––––––––––––––

	$('.footer_facebook_link').click(function() {
		// Send "Social Media - Facebook - Footer" event to Google Analytics.
		ga('send', 'event', 'Social Media', 'Facebook', 'Footer');
	});

	$('.footer_twitter_link').click(function() {
		// Send "Social Media - Twitter - Footer" event to Google Analytics.
		ga('send', 'event', 'Social Media', 'Twitter', 'Footer');
	});

	$('.footer_instagram_link').click(function() {
		// Send "Social Media - Instagram - Footer" event to Google Analytics.
		ga('send', 'event', 'Social Media', 'Instagram', 'Footer');
	});

	$('.sidebar_facebook_link').click(function() {
		// Send "Social Media - Facebook - Footer" event to Google Analytics.
		ga('send', 'event', 'Social Media', 'Facebook', 'Sidebar');
	});

	$('.sidebar_twitter_link').click(function() {
		// Send "Social Media - Twitter - Footer" event to Google Analytics.
		ga('send', 'event', 'Social Media', 'Twitter', 'Sidebar');
	});

	$('.sidebar_instagram_link').click(function() {
		// Send "Social Media - Instagram - Footer" event to Google Analytics.
		ga('send', 'event', 'Social Media', 'Instagram', 'Sidebar');
	});

});