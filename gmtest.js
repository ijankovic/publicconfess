//var fs = require('fs')
var gm = require('gm').subClass({imageMagick: true});
var async = require('async');


// resize and remove EXIF profile data
// gm('C:/Users/Ivan/DSC02003.JPG')
// .resize(240, 240)
// .noProfile()
// .write('resize.png', function (err) {
//   if (err) {
//   	console.log("It's not working");
//   	console.log(err);
//   } else {
//   	console.log("It's working");
//   }
// });

// gm(400, 400, "#0E74AF")
// .fontSize(24)
// .drawText(30, 20, "Iskren da budem nije mi jasno kako ce ovo raditi. Zato sto verovatno nece biti mesta da se bila sta napravi na ovaj fazon.")
// .write("resize.png", function (err) {
//   if (err) {
//   	console.log("It's not working");
//   	console.log(err);
//   } else {
//   	console.log("It's working");
//   }
// });


// 640px
// 


// gm(1024, 1024, "#0E74AF")
// .command("convert") 
// .in("-background", "transparent")
// // .in("-fill", "#FFFFFF")
// .in("-size", "900x900")
// .in("-fill", "white")
// // .in("-density", "125")
// .in("-font", "Open-Sans")
// //.in("-pointsize", "32")
// //.in('caption:"Dolazim kuci sa posla, a u hodniku nema svetla i patim se da stavim kljuc u bravu kad cujem kako moja zena u stanu stenje. Ja pozurio da otkljucam vrata, kontam ona sa drugim u krevetu, kad ona stoji sama. To joj je odbrana da neko ne provali u stan, kontat ce ljudi kao da smo oboje tu pa nece provaliti. I dokle sad mogu ovako da pisem. Koliko ima mesta? Kada ce biti dosta. Koliko cu moci jos da pisem. Interesuje me to. Mislim da smo sad stigli do same granice jer vise nema prostora."')
// .in('caption:“Da nije muzike davno bi poludila.”')
// //.in('caption:“Zaposlio sam se preko leta, od para koje sam ustedeo, uplatio sam (samohranom) ocu dvonedeljno letovanje u Egiptu. Na aerodromu kad smo se rastajli samo sam mu pozeleo da se lepo provede. Kad sam stigao kuci, posalo sam mu sms da ga volim i da se cuva. To mi je bio prvi put da sam mu rekao da ga volim sa svojih 18 godina, iako mi je on sve ne svetu. Voleo bih da imam snage da mu to kazem uzivo.”')
// // .out("+antialias")
// .in("-gravity", "Center")
// //.in("-geometry", "+150+0")
// .out("-composite")
// .resize(512, 512)
// .write( "seja2.png", function (err) {
//   if (err) {
//   	console.log("It's not working 2");
//   	console.log(err);
//   } else {
//   	console.log("It's working 2");
//   }
// });



// -background lightblue  -fill blue  -font Corsiva -pointsize 36 \
//           -size 320x   caption:'This is a very long caption line.

// var text = "“Da nije muzike davno bi poludila.”";
// var text = "Umesto mobilnog na krevet sam bacio solju sa kafom.";
// //var text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent bibendum consectetur felis quis pretium. Sed eget odio pharetra, aliquam mauris non, mollis diam. Duis fermentum quis metus sit amet scelerisque. Mauris et nisi quis enim vulputate gravida. Sed sit amet luctus nisi, in accumsan orci. Vivamus nec est in nisi condimentum scelerisque nec rutrum felis. Sed sollicitudin justo sit amet purus sollicitudin facilisis."

// //gm()
// gm()
// .command("convert")
// .in('POSTER_RECTANGLE.png')
// .in("-gravity", "Center")
// .in("-size", "880x")
// .in("-background", "transparent")
// .in("-fill", "#ffffff")
// .in("-font", "Open-Sans")
// .in("-pointsize", "48")
// .in('caption:' + text)
// // .in("-gravity", "Center")
// .in("-geometry", "+0-46")
// .out("-composite")
// //.resize(512, 512)
// .write( "023.png", function (err) {
//   if (err) {
//     console.log("It's not working 3");
//     console.log(err);
//   } else {
//     console.log("It's working 3");
//   }
// });

var confessions = [
"When everybody was asleep in the kindergarten, I just had my eyes closed!",
"I go for a run only hoping to meet a pretty girl who also runs.",
"I've always been serious, even a bit grumpy, I've never been one of those cheerful and laughing girls. And I'd so like to be...",
"It's so sad and pathetic when someone uses the excuse of I didn't get your text or I sent you a text but you must not have gotten it. If you don't want to talk to someone, say that.",
"Two months ago I realized I could not pass my university exams. Im thinking about dropping out and enrolling somewhere else, but my parents dont even wanna hear about it. It seems that they find it more important what others think and I can struggle with topics that I cant even read anymore...",
"Every day when we come home from work my husband and I drink coffee. The silence that fills the room hurts me more with every day. In silence, each in our own world, we drink coffee and then each goes to do their own thing. I feel like that silence breaks my heart into 1000 pieces. :-(",
"It bothers me that I don't have anyone who would tell me honestly if I'm ugly or not. So I wouldn't come up to girls for no reason, on top of putting them in an awkward situation, I go through hell in the process of approaching them...",
"We have a teaching assistant at university who's only a few years older than us and he's really hot. All the students hit on him and he just gives them the cold shoulder, he doesn't even want to talk to anyone, he's really arrogant. Once when we had practice I ended up alone with him and he was the exact opposite of all of the above, we talked about hobbies, family, he even asked me out for a drink which I declined. After a few days when we were with everyone else in the group he became closed off and cold again, and I'm just curious as to what he's really like, the way he was when we were alone or the way he is when we're with everyone else.",
"I got terribly embarrassed today. As it were, we had our neighbour's daughter over and since they kept offering her this and that to eat, she said the following: I don't want to eat and end up having a stomach like her and pointed at me. I am a little overweight, just so we're clear, but I still have trouble believing how much a seven-year-old can embarrass me. :/",
"I wish I had the same determination in the morning that I have in the evening, usually in the evening I have the will to stop smoking and start a healthy life, but in the morning it all just goes away and the only thing that's left is a memory.",
"I have a quiet voice and nobody takes me seriously.",
"It annoys me when I play a video or a song for someone to see/hear and they speed through it or stop it halfway through.",
"A co-worker from my boyfriend's work keeps texting him, boring him, hitting on him, she's married and she looks like an IV stand. Clearly her husband is not pleasing her so she's now stuck on my boyfriend. And it's driving me insane even though she looks like an IV stand. So I'd kindly ask her husband to fulfill his marital duties, I can't get irritated over idiots.",
"I'm getting married in three months and with every day that passes I want to break up more, I don't know if it's nerves or if it's for real...",
"This summer we had to put our dog down because of a tumor. I'd never experienced greater pain than while my best friend grew weaker day by day. I'll never forget those eyes that have closed forever. I'll forever remember the day he followed me home and simply crawled into my heart and stayed there forever. The pure love between an owner and a dog cannot be described in words and the void that's left nothing can fill. Even now I hope he's in a better place with tears in my eyes.",
"I don't know how to enjoy in the moment, I only think of the future and the consequences, never of what is happening to me in this exact moment, in the present. And then, when that moment passes, I wonder why I didn't use it better and enjoy.",
"I have a handsome dentist and once when I was supposed to go for a check-up, I got all done up, put my makeup on and halfway there remembered that I forgot the most important thing... to brush my teeth.",
"The best meeting of my laziness and my sleepiness during high school was when I put clothes on on top of my pajamas and went to school like that. As soon as I'd get inside the house, I'd take my clothes off, stay in my pajamas and go to sleep like that.",
"I'll have been in a relationship with a girl for 4 months tomorrow. Of course it's about time to introduce her to my parents so I tell them to clean up a little, not to be in their old and dirty clothes in the afternoon because I'm bringing my girlfriend. So I walk in, open the door for her, tell her about them and my old man aproaches us first in a suit and tie and shoes and then ma comes up to us in a wedding dress. If she didn't dump me then, she never will.",
"When I go to take a shower I spend 90% of the time posing in front of the mirror and 10% showering.",
"I love it when I go to a coffee shop and my cellphone connects to the wifi immediately. I feel like I just walked into my own house and I'm immediately more comfortable.",
"I just don't get it... Some say to wait for nothing, life passes by quickly, live it... Others say that in life you have to be patient because the best things take time. So what should I do now, live my life or wait...",
"Life is funny, after high school I decided not to enroll into university because I thought it was a waste of time, my father wanted to denounce me for that and said I was the biggest letdown this family's ever had, we didn't talk properly for 2 years, I recently became a co-owner of a Belgrade inn and a month after that I got a text from my father asking if I could lend him some money for a while. Don't look down on people who don't have a higher education because it is the biggest misconception that you're only someone and something if you finish university.",
"There are girls I become friends with only so I can hurt them, first I charm them so they trust me, and when they tell me some of their private things and problems, I use that against. There's no better feeling than when I see those eyes filled with tears and that suffering in their face and their broken soul but I only do it to make them stronger because there are much worse people than me out there and I do it to protect them, I'm a good man.W",
"I'm 50 and have more plans for the future than I had when I was 20.",
"For couple of nights in  row I have the same dream which continues every night. It seems that it could become real and that scares me a bit.",
"I'm 23 and I'm fighting depression. I've managed to push away all my friends and the man who I love with all my heart and with whom I had everything I imagined my future. I am completely alone. I'm not a bad person, I just don't believe in myself. I don't know how and I don't have the strength to move on.",
"I'm secretly in a relationship with my brother's friend. We fell in love, even though it's wrong we can't stop because we are having such a good time. ",
"Whenever I go somewhere, I have to check three times to see if I brought everything I need, because I always think I had forgotten something. ",
"I have recently found out that I can't have children. He and I were together before I had found out that. The more I try to push him away with my bad behavior the better he treats me. I don't have the strength to break his heart. If I leave him I'll hurt him. If I confess to him, I'll hurt him. And if I go on without telling him, I'll still hurt him...",
"When I was a little I couldn't wait to be an adult. To go out wherever,  wherever and with whoever I want. Now I'm graduating from college, I can do whatever I want but I'm completely lost because I'm not sure what I want. If only I were a child again.",
"My girlfriend can find anything to be a problem. We started our relationship 4 years ago, I was supposed to go abroad to work, but she begged me to stay because it would be the end of our relationship. Two years ago she started having problem with my look, so I started going to gym, to please her. Now she's having problem with my job, she wants me to go abroad, saying that I'm not ambitious enough for her. i don't have a car and that bothers her. She only talks how she will go far with her bachelor's degree. When I say I can't afford something she starts talking about my job. I'm feeling suffocated and not pleasant at all in this relationship, especially because I have to female friends with whom I talk about this and who understand me. They think I look good, that my job is OK, that I'm a great catch. They tell me to leave her because every girl would want to date me. I'm thinking about a break up more and more. I want a girl who respects me, for who I am, not a girl who wants to make me her trophy boyfriend. "
]

// for (var i = 0; i < confessions.length; i++) {
//   var content = confessions[i];



// var x = content.length;
// var text_size = 0;

// console.log("Confession " + i + " has " + x + " characters.");
//   switch (true) {
//     case (x < 50):
//         text_size = 48;
//         break;
//     case (x >= 50 && x < 100):
//         text_size = 46;
//         break;
//     case (x >= 100 && x < 150):
//         text_size = 44;
//         break;
//     case (x >= 150 && x < 200):
//         text_size = 42;
//         break;
//     case (x >= 200 && x < 250):
//         text_size = 42;
//         break;
//     case (x >= 250 && x < 300):
//         text_size = 42;
//         break;
//     case (x >= 300 && x < 350):
//         text_size = 42;
//         break;
//     case (x >= 350 && x < 400):
//         text_size = 42;
//         break;
//     case (x >= 400 && x < 450):
//         text_size = 42;
//         break;
//     case (x >= 450 && x < 540):
//         text_size = 42;
//         break;
//     case (x >= 540):
//         text_size = 10;
//         console.log("Confession has over 540 characters. NO POSTER!");
//     }


//         console.log("Text size for confession " + i + " is " + text_size);


//     gm()
//   .command("convert")
//   .in('POSTER_RECTANGLE.png')
//   .in("-gravity", "Center")
//   .in("-size", "880x")
//   .in("-background", "transparent")
//   .in("-fill", "#ffffff")
//   .in("-font", "Open-Sans")
//   .in("-pointsize", text_size)
//   .in('caption:' + content)
//   // .in("-gravity", "Center")
//   .in("-geometry", "+0-46")
//   .out("-composite")
//   //.resize(512, 512)
//   .write( "000" + i +".png", function (err) {
//     if (err) {
//       console.log("It's not working " + i);
//       console.log(err);
//     } else {
//       console.log("It's working " + i);
//     }
//   });


// }

var i = 0;

async.eachSeries(confessions, function(confession, callback) {

  // Perform operation on file here.
  //console.log(confession);
var content = confession;
var x = content.length;
var text_size = 0;

var base_size = 44;

console.log("Confession " + i + " has " + x + " characters.");
  switch (true) {
    case (x <= 50):
        //text_size = 46;
        text_size = base_size + 16;
        break;
    case (x > 50 && x <= 100):
        //text_size = 44;
        text_size = base_size + 14;
        break;
    case (x > 100 && x <= 150):
        //text_size = 42;
        text_size = base_size + 12;
        break;
    case (x > 150 && x <= 200):
        //text_size = 40;
        text_size = base_size + 10;
        break;
    case (x > 200 && x <= 250):
        //text_size = 38;
        text_size = base_size + 8;
        break;
    case (x > 250 && x <= 300):
        //text_size = 36;
        text_size = base_size + 6;
        break;
    case (x > 300 && x <= 350):
        //text_size = 34;
        text_size = base_size + 4
        break;
    case (x > 350 && x <= 400):
        //text_size = 32;
        text_size = base_size + 2
        break;
    case (x > 400 && x <= 450):
        //text_size = 30;
        text_size = base_size;
        break;
    case (x > 450):
        //text_size = 30;
        text_size = base_size;
        content = content.substring(0, 444);
        last_space = content.lastIndexOf(" ");
        content = content.substring(0, last_space);

        content += " . . . ";
        //content += "XOXOXO";
        break;
  }


        console.log("Text size for confession " + i + " is " + text_size);
        


    gm()
  .command("convert")
  .in('POSTER_SQUARE.png')
  .in("-gravity", "Center")
  .in("-size", "880x")
  .in("-background", "transparent")
  .in("-fill", "#ffffff")
  .in("-font", "Open-Sans-Semibold")
  .in("-pointsize", text_size)
  .in('caption:' + content)
  // .in("-gravity", "Center")
  .in("-geometry", "+0-72")
  .out("-composite")
  //.resize(512, 256)
  //.resize(640, 640)
  //.resize(305, 305)
  .write( "100" + i +".png", function (err) {
    if (err) {
      console.log("It's not working " + i);
      console.log(err);
    } else {
      console.log("It's working " + i + "\n\n");
      i++;
      callback();
    }
  });

}, function(err){
    // if any of the file processing produced an error, err would equal that error
    if( err ) {
      // One of the iterations produced an error.
      // All processing will now stop.
      console.log('A file failed to process');
    } else {
      console.log('All files have been processed successfully');
    }
});