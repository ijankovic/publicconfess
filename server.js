// server.js

// IMPORT PACKAGES
// ===================================================

// Load external packages
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var CronJob = require('cron').CronJob;
var path = require('path');
var moment = require('moment');

// Load internal packages
var scheduledTasks = require('./app/scheduled_tasks');
var utils = require('./app/utils');



// BASE SETUP
// ===================================================

// Create express application
var app = express();

// Use the body-parser package in our application
app.use(bodyParser.urlencoded({
  extended: true
}));

// Disable x-powere-by HTTP header for safety reason.
// Otherwise every response would have x-powered-by HTTP header informing users that server is running Express.
app.disable('x-powered-by');


// Serve static content for the app from the "public" directory in the application directory.
// __dirname is directory of server.js
var public_dir = __dirname + '/public';	// where public content (html, css js) is stored.
var oneDay = 86400000; // one day in milliseconds

// Cache static content for one day.
app.use(express.static(public_dir, { maxAge: oneDay }));
//app.use(express.static(public_dir));



// DATABASE
// ===================================================

//var mongodbUri = "mongodb://localhost/publicconfess_db";																// local database
var mongodbUri = "mongodb://admin:ispovesti@ds031611.mlab.com:31611/heroku_app33269567";	// remote database
// connect to the database on default port 27071
mongoose.connect(mongodbUri, function(error) {
	if (error) {
		console.log("Unable to connect to the database.");
		console.log(error);
	}
});



// ROUTES
// ===================================================

var api_router = express.Router();
var public_router = express.Router();

// Import public routes
require('./app/routes/public')(public_router, public_dir);
require('./app/routes/tools')(public_router, public_dir);

// Import api routes
require('./app/routes/pending_confessions')(api_router);
require('./app/routes/confessions')(api_router);
require('./app/routes/comments')(api_router);
require('./app/routes/send_email')(api_router);

// Invoked for any api requests.
// Security check to pass only Ajax requests to API.
app.all('/api/*', function(req, res, next) {

	// Check if request is Ajax
	if (req.xhr) {
		// Forward request to the next router.
		next();
	} else {
		console.log("Somebody is trying to access api by not using ajax request.");
		// Send 403 Forbidden HTTP status code in response to a request from a client
		// for a web page or resource to indicate that the server can be reached and
		// understood the request, but refuses to take any further action.
		res.status(403).end();
	}
});

// Register routes.
app.use('/api', api_router);	// api_router routes have /api prefix
app.use('/', public_router);	// public_router routes have / prefix



// // CRON JOBS
// // ===================================================

// // Use environment defined trend period or default to 3 hours.
// var trendPeriod = process.env.TREND_PERIOD || 3;

// var cron_pattern_popular_check = '00 00 */' + trendPeriod + ' * * *';
// var cron_pattern_share_popular = '00 00 1,4,7,10,13,16,19,22 * * *'; // run at specific hours
// var cron_pattern_share_instagram = '00 00 5,17 * * *'; // run at specific hours
// var cron_pattern_post_premade = '00 05 */3 * * *'; // run 5 minutes after every 3 hours


// // Cron job which flags popular confessions.
// var jobPopularCheck = new CronJob(cron_pattern_popular_check , function() {
// 	scheduledTasks.popularityCheck(trendPeriod);
// }, null, true, "America/Toronto");


// // Cron job which posts to Facebook and Twitter.
// var jobSharePopular = new CronJob(cron_pattern_share_popular , function() {
// 	scheduledTasks.publishToSocialMedia(public_dir);
// }, null, true, "America/Toronto");


// // Cron job which creates poster for Instagram.
// var jobShareInstagram = new CronJob(cron_pattern_share_instagram , function() {
// 	scheduledTasks.shareOnInstagram();
// }, null, true, "America/Toronto");


// // Cron job which publishes premade content.
// var jobPostPremade = new CronJob(cron_pattern_post_premade , function() {
// 	scheduledTasks.postPremade();
// }, null, true, "America/Toronto");



// // JOBS TO RUN ON SERVER START
// // ===================================================

// // Run popularityCheck immediately on server start.
// utils.getLatestTime("popularityCheck", function(err, timestamp) {

// 	if (!err) {
		
// 		// Create a moment from the timestamp of the most recent popularityCheck cron job.
// 		var analysis_time = moment(timestamp);

// 		// Run popularity check for the analysis trend time provided.
// 		scheduledTasks.popularityCheck(trendPeriod, analysis_time);
// 	}
// });

// // Run premade confession posting job from premade queue on server start.
// scheduledTasks.checkPremadeQueue();



// START THE SERVER
// ===================================================

// Use environment defined port or 8080
var port = process.env.PORT || 8080;

app.listen(port);
console.log('Magic happens on port ' + port);