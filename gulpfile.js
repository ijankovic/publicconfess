var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var uglifyJS = require('gulp-uglify');

gulp.task('css', function() {
	return gulp.src(['public/css/normalize.css', 'public/css/main.css', 'public/css/icons.css', 
					 'public/css/dialog.css', 'public/css/dialog-sally.css'], {base: 'public/css/'})
				.pipe(concat('site.css'))
				.pipe(minifyCSS())
				.pipe(gulp.dest('public/css'))
});

gulp.task('js', function() {
	return gulp.src('public/js/main.js')
				.pipe(uglifyJS())
				.pipe(rename('main.min.js'))
				.pipe(gulp.dest('public/js'))
});

gulp.task('default', ['css', 'js'], function() {});