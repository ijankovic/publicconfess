def filter_chars(msg, keeps=None):
    
    new_message = msg
    
    if keeps:
        for c in msg:
            if c not in keeps:
                new_message.replace(c, "")
    else:
        new_message = new_message.replace(" ", "")
        new_message = new_message.replace(".", "")
        new_message = new_message.replace(",", "")
        new_message = new_message.replace(":", "")
        new_message = new_message.replace(";", "")
        new_message = new_message.replace("?", "")
        new_message = new_message.replace("!", "")
        new_message = new_message.replace(")", "")
        new_message = new_message.replace("(", "")        
                
    return new_message



print filter_chars("hi there! :)","ie:")