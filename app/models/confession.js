// app/models/confession.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ConfessionSchema = new Schema({
	_id: Number,
	content: String,
	approve: Number,
	condemn: Number,
	num_comments: Number,
	fb_like: Number,
	tweet: Number,
	popularity: Number,
	popular_flag: Boolean,
	timestamp: String
}, { collection: 'confessions', versionKey: false });
// Mongoose will create collection automatically by adding s to 'Confession'.

// Export the Mongoose model
module.exports = mongoose.model('Confession', ConfessionSchema);