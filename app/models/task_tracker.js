// app/models/task_tracker.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskTrackerSchema = new Schema({
	_id: String,			// Name of the task. (e.g. popularityCheck)
	timestamp: String		// Timestamp of the task.
}, { collection: 'task_tracker', versionKey: false });

// Export the Mongoose model
module.exports = mongoose.model('TaskTracker', TaskTrackerSchema);