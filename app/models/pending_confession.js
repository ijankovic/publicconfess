// app/models/pending_confession.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PendingConfessionSchema = new Schema({
	_id: Number,
	content: String,
	yes: Number,
	no: Number,
	report: Number,
	suspicious: Boolean,
	timestamp: String
}, { collection: 'pending_confessions', versionKey: false });
// Specify which collection to use. Use collection named 'pending_confessions'.

// Export the Mongoose model
module.exports = mongoose.model('PendingConfession', PendingConfessionSchema);