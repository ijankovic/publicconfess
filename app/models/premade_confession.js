// app/models/pending_confession.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PremadeConfessionSchema = new Schema({
	_id: Number,
	real_id: Number,
	content: String,
	comments: [String] 
}, { collection: 'premade_confessions', versionKey: false });
// Specify which collection to use. Use collection named 'premade_confessions'.

// Export the Mongoose model
module.exports = mongoose.model('PremadeConfession', PremadeConfessionSchema);