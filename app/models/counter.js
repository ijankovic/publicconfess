// app/models/counter.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CounterSchema = new Schema({
	_id: String,
	seq: Number
}, { collection: 'counters'});
// Specify which collection to use. Use collection named 'counters'.

// Export the Mongoose model
module.exports = mongoose.model('Counter', CounterSchema);