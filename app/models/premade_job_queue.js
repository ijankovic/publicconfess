// app/models/premade_job_queue.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PremadeJobQueueSchema = new Schema({
	scheduled_timestamp: String,
	post_type: String,
	premade_id: Number,
}, { collection: 'premade_job_queue', versionKey: false });
// Specify which collection to use. Use collection named 'premade_job_queue'.

// Export the Mongoose model
module.exports = mongoose.model('PremadeJobQueue', PremadeJobQueueSchema);