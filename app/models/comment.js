// app/models/comment.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = new Schema({
	_id: Number,			// id of comment
	reply_to_id: Number,	// id of confession or comment
	reply_to: String,		// confession or comment
	likes: Number,
	dislikes: Number,
	report: Number,
	author: String,
	content: String,
	timestamp: String,
	suspicious: Boolean
}, { collection: 'comments', versionKey: false });

// Export the Mongoose model
module.exports = mongoose.model('Comments', CommentSchema);