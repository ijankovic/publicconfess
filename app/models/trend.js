// app/models/trend.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TrendSchema = new Schema({
	_id: Number,
	timestamp: String,
	trend: Number
}, { collection: 'trends', versionKey: false });

// Export the Mongoose model
module.exports = mongoose.model('Trend', TrendSchema);