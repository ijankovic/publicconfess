// Load packages
var async = require('async');
var moment = require('moment');
var CronJob = require('cron').CronJob;

// Load controllers
var confessionController = require('./controllers/confession');
var trendController = require('./controllers/trend');
var premadeContentController = require('./controllers/premade_content');

// Load models
var PremadeJobQueue = require('./models/premade_job_queue');

// Load internal packages
var utils = require('./utils');
var poster = require('./poster');
var socialShare = require('./social_share');


// Global variables
// ===================================================

var flagPopularJobs = [];		// contains cron jobs.
var confessionsToPublish = [];	// contians confession objects corresponding to cron jobs.

var postPremadeConfessionsJobs = [];	// contains cron jobs.
var premadeConfessionsToPublish = [];	// contains premade confession objects corresponding to cron jobs.

var postPremadeCommentsJobs = [];		// contains cron jobs.
var premadeCommentsToPublish = []; 		// contains premade comment objects corresponding to cron jobs.


// Helper functions
// ===================================================

function addConfessionToPremadeJobQueue(premade_confession_id, scheduled_timestamp) {

	// Create a new instance of the PremadeJobQueue model
	var premadeJobQueue = new PremadeJobQueue();

	premadeJobQueue.premade_id = premade_confession_id;
	premadeJobQueue.post_type = "confession";
	premadeJobQueue.scheduled_timestamp = scheduled_timestamp.toISOString();

	// Save the premadeJobQueue and check for errors
	premadeJobQueue.save(function(err) {
		if (err) {
			console.log("Error trying to add confession to premade job queue: " + err);
		} else {
			console.log("Added entry for premade confession #" + premade_confession_id + " to premade job queue.");
		}
	});
}


function removeConfessionFromPremadeJobQueue(premade_confession_id) {

	PremadeJobQueue.remove({ premade_id: premade_confession_id }, function (err) {
		if (err) {
			console.log("Error trying to remove premade job queue" + err);
		} else {
			console.log("Removed entry for premade confession #" + premade_confession_id + " from premade job queue.");
		}
	});
}


function publishPremadeConfessionAndComments(premade_confession_id) {

	premadeContentController.getPremadeConfession(premade_confession_id, function(premade_confession) {

		// Push related confession object to premadeConfessionsToPublish array.
		premadeConfessionsToPublish.push(premade_confession);

		// Get the first related confession object from premadeConfessionsToPublish array.
		var premade_confession_to_publish = premadeConfessionsToPublish.shift();

		console.log("Publishing premade confession " + premade_confession_to_publish._id + ".");

		// Publish the premade confession.
		premadeContentController.publishPremadeConfession(premade_confession_to_publish, function(confession) {

			removeConfessionFromPremadeJobQueue(premade_confession_to_publish._id);

			// At the moment of confession publishing we need to immediately schedule comments publishing.
			premadeContentController.getPremadeComments(premade_confession_to_publish._id, function(comments) {

				if (comments) {

					// Go through each comment and schedule a cron job which will publish the comment.
					for (var i = 0; i < comments.length; i++) {

						// Make sure that comment is not empty
						if (comments[i] != "") {

							var comment = {};
							comment.content = comments[i];
							comment.reply_to_id = confession._id;

							var postponeMinutes = utils.getRandomInt(20, 120);	// comments are going to be published between 20 and 120 minutes after confession.

							var publish_comment_timestamp = moment.utc(confession.timestamp).add(postponeMinutes, 'minutes');

							console.log("Scheduling publish comment cron job for comment[" + i + "] at " + publish_comment_timestamp.toISOString() +
										" for premade confession #" + premade_confession_to_publish._id + " which is now confession #" + confession._id);


							var premadeCommentCronTime = publish_comment_timestamp.seconds() + ' ' +
												publish_comment_timestamp.minutes() + ' ' +
												publish_comment_timestamp.hours() + ' ' +
												publish_comment_timestamp.date() + ' ' +
												publish_comment_timestamp.month() + ' *';

							// Create new cron job
							var publishPremadeComment = new CronJob({
								cronTime: premadeCommentCronTime,
								onTick: function() {

									// Get the first related comment object and remove it from premadeCommentsToPublish array.
									var commentToPublish = premadeCommentsToPublish.shift();

									console.log("Publishing premade comment for confession #" + confession._id);

									premadeContentController.publishPremadeComment(commentToPublish);

									// Stop the cron job since we do not need it anymore.
									this.stop();
									// Remove the cron job from postPremadeCommentsJobs array.
									postPremadeCommentsJobs.shift();

								},
								start: false,
								timeZone: 'UTC'
							});

							// Push the newly created cron job to postPremadeCommentsJobs array.
							postPremadeCommentsJobs.push(publishPremadeComment);

							// Push related confession object to confessionsToPublish array.
							premadeCommentsToPublish.push(comment);

						}
					}

					// Once all the cron jobs have been created and pushed to postPremadeCommentsJobs array, we can start them.
					console.log("Starting publish premade comments cron jobs...");
					for (var k = 0; k < postPremadeCommentsJobs.length; k++) {
						postPremadeCommentsJobs[k].start();
					}
				}
			}); 
		});
	});
}


// Export functions
// ===================================================


var popularityCheck = function (trendPeriod, analysis_time) {

	// If analysis_time parameter is not passed, we will use the current time.
	analysis_time = typeof analysis_time !== 'undefined' ? analysis_time : moment.utc();

	var cron_job_start = moment.utc().toISOString();
	console.log("popularityCheck cron job started at " + cron_job_start);
	console.log("Trend period is " + trendPeriod + " hours.");

	// Analyze confessions posted between trendPeriod * 2 hours ago and trendPeriod hours ago.
	var trend_start = moment(analysis_time).subtract(trendPeriod * 2 , 'hours');
	var trend_end = moment(analysis_time).subtract(trendPeriod, 'hours');

	// Reset minutes, seconds and milliseconds to 0.
	trend_start.set('minute', 0);
	trend_start.set('second', 0);
	trend_start.set('millisecond', 0);

	trend_end.set('minute', 0);
	trend_end.set('second', 0);
	trend_end.set('millisecond', 0);

	trend_start_timestamp = trend_start.toISOString();
	trend_end_timestamp = trend_end.toISOString();

	console.log("Analyzing trend for confessions published between the following timestamps:");
	console.log("trend_start:\t" + trend_start_timestamp);
	console.log("trend_end:\t" + trend_end_timestamp);

	trendController.getPopular(trend_start_timestamp, trend_end_timestamp, function(results) {

		if (results) {

			// Go through each confession and schedule a cron job which will set confession's popular flag to popular.
			for (var i = 0; i < results.length; i++) {

				var popularConfession = results[i];

				// Confession will be set to popular at twice the trendPeriod plus 1 hour time after publish time. 
				var flag_popular_timestamp = moment.utc(popularConfession.timestamp).add(trendPeriod * 2 + 1 , 'hours');

				console.log("Scheduling flag popular cron job for confession #" + popularConfession._id + " at " + flag_popular_timestamp.toISOString());

				var flagCronTime = flag_popular_timestamp.seconds() + ' ' +
									flag_popular_timestamp.minutes() + ' ' +
									flag_popular_timestamp.hours() + ' ' +
									flag_popular_timestamp.date() + ' ' +
									flag_popular_timestamp.month() + ' *';

				// Create new cron job
				var flagJob = new CronJob({
					cronTime: flagCronTime,
					onTick: function() {

						// Get the first related confession object and remove it from confessionsToPublish array.
						var confession = confessionsToPublish.shift();

						// Get id of the first confession in confessionsToPublish array and make the confession popular.
						confessionController.setPopularFlag(confession._id);

						// Stop the cron job since we do not need it anymore.
						this.stop();
						// Remove the cron job from flagPopularJobs array.
						flagPopularJobs.shift();

					},
					start: false,
					timeZone: 'UTC'
				});

				// Push the newly created cron job to flagPopularJobs array.
				flagPopularJobs.push(flagJob);

				// Push related confession object to confessionsToPublish array.
				confessionsToPublish.push(popularConfession);
			}

			// Once all the cron jobs have been created and pushed to flagPopularJobs array, we can start them.
			console.log("Starting flag popular cron jobs...");
			for (var k = 0; k < flagPopularJobs.length; k++) {
				flagPopularJobs[k].start();
			}

		}

		// Update popularityCheck task timestamp in task tracker.
		utils.updateLatestTime("popularityCheck", cron_job_start, function() {
			console.log("popularityCheck cron job COMPLETE!");
		});
	});
}


var publishToSocialMedia = function(public_dir) {

	var cron_job_start = moment.utc();
	console.log('publishToSocialMedia cron job started at ' + cron_job_start.toISOString());

	// Analyze confessions posted between 25 and 28 hours ago.
	var published_from = moment.utc().subtract(28, 'hours');
	var published_to = moment.utc().subtract(25, 'hours');

	// Reset minutes, seconds and milliseconds to 0.
	published_from.set('minute', 0);
	published_from.set('second', 0);
	published_from.set('millisecond', 0);

	published_to.set('minute', 0);
	published_to.set('second', 0);
	published_to.set('millisecond', 0);

	published_from_timestamp = published_from.toISOString();
	published_to_timestamp = published_to.toISOString();

	console.log("Analyzing confessions published between the following timestamps:");
	console.log("published_from:\t" + published_from_timestamp);
	console.log("published_to:\t" + published_to_timestamp);

	trendController.getMostPopular(published_from_timestamp, published_to_timestamp, function(results) {

		if (results.length > 0) {

			var best_confession = results[0];

			// Get confession content.
			confessionController.read_resource(best_confession._id, function(err, confession) {
				console.log("Best confession:");
				console.log(confession);

				// Create the poster.
				poster.createRectanglePoster(confession.content, public_dir, function(err) {
					if (!err) {

						// Publish to social media
						socialShare.shareLinkOnFacebook(confession);
						socialShare.shareOnTwitter(public_dir, confession);
					}
				});
			});
		} else {
			console.log("No confessions found in the given time range. There is no best confession.");
		}
	});
}


var shareOnInstagram = function() {

	var cron_job_start = moment.utc();
	console.log('shareOnInstagram cron job started at ' + cron_job_start.toISOString());

	// Analyze confessions posted between 29 and 17 hours ago.
	// Note that shareOnInstagram job will run at 5:00 and 17:00.
	// We want to select confessions between 00:00 and 12:00, 12:00 and 00:00 respectively.
	var published_from = moment.utc().subtract(29, 'hours');
	var published_to = moment.utc().subtract(17, 'hours');

	// Reset minutes, seconds and milliseconds to 0.
	published_from.set('minute', 0);
	published_from.set('second', 0);
	published_from.set('millisecond', 0);

	published_to.set('minute', 0);
	published_to.set('second', 0);
	published_to.set('millisecond', 0);

	published_from_timestamp = published_from.toISOString();
	published_to_timestamp = published_to.toISOString();

	console.log("Analyzing confessions published between the following timestamps:");
	console.log("published_from:\t" + published_from_timestamp);
	console.log("published_to:\t" + published_to_timestamp);


	trendController.getMostPopular(published_from_timestamp, published_to_timestamp, function(results) {

		if (results.length > 0) {

			var best_confession = results[0];

			// Get confession content.
			confessionController.read_resource(best_confession._id, function(err, confession) {
				console.log("Best confession:");
				console.log(confession);

				// Create the poster.
				poster.createSquarePosterDropbox(confession.content);
			});
		} else {
			console.log("No confessions found in the given time range. There is no best confession.");
		}
	});
}


var postPremade = function() {

	premadeContentController.getNextPremadeConfession(function(premade_confession) {

		if (premade_confession) {

			var randHourIncrease = utils.getRandomInt(0, 2);		// generate random hour to post after cron job start.
			var randMinuteIncrease = utils.getRandomInt(0, 50);		// generate random minute to post after cron job start.
			var randSecondIncrease = utils.getRandomInt(0, 59);		// generate random second to post after cron job start.

			var post_premade_timestamp = moment.utc().add(randHourIncrease , 'hours').add(randMinuteIncrease , 'minutes').add(randSecondIncrease , 'seconds');

			// Add scheduled premade confession to job queue.
			addConfessionToPremadeJobQueue(premade_confession._id, post_premade_timestamp);

			console.log("Scheduling post premade confession cron job for premade confession #" + premade_confession._id + " at " + post_premade_timestamp.toISOString());

			var premadePostCronTime = post_premade_timestamp.seconds() + ' ' +
								post_premade_timestamp.minutes() + ' ' +
								post_premade_timestamp.hours() + ' ' +
								post_premade_timestamp.date() + ' ' +
								post_premade_timestamp.month() + ' *';

			// Create new cron job
			var postPremadeJob = new CronJob({
				cronTime: premadePostCronTime,
				onTick: function() {

					// Get the first related confession object from premadeConfessionsToPublish array.
					var premade_confession_to_publish = premadeConfessionsToPublish.shift();

					console.log("Publishing premade confession " + premade_confession_to_publish._id + ".");

					// Publish the premade confession.
					premadeContentController.publishPremadeConfession(premade_confession_to_publish, function(confession) {

						removeConfessionFromPremadeJobQueue(premade_confession_to_publish._id);

						// At the moment of confession publishing we need to immediately schedule comments publishing.
						premadeContentController.getPremadeComments(premade_confession_to_publish._id, function(comments) {

							if (comments) {

								// Go through each comment and schedule a cron job which will publish the comment.
								for (var i = 0; i < comments.length; i++) {

									// Make sure that comment is not empty
									if (comments[i] != "") {

										var comment = {};
										comment.content = comments[i];
										comment.reply_to_id =  confession._id;

										var postponeMinutes = utils.getRandomInt(20, 120);	// comments are going to be published between 20 and 120 minutes after confession.
		 
										var publish_comment_timestamp = moment.utc(confession.timestamp).add(postponeMinutes, 'minutes');

										console.log("Scheduling publish comment cron job for comment[" + i + "] at " + publish_comment_timestamp.toISOString() +
													" for premade confession #" + premade_confession_to_publish._id + " which is now confession #" + confession._id);

										var premadeCommentCronTime = publish_comment_timestamp.seconds() + ' ' +
															publish_comment_timestamp.minutes() + ' ' +
															publish_comment_timestamp.hours() + ' ' +
															publish_comment_timestamp.date() + ' ' +
															publish_comment_timestamp.month() + ' *';

										// Create new cron job
										var publishPremadeComment = new CronJob({
											cronTime: premadeCommentCronTime,
											onTick: function() {

												// Get the first related comment object and remove it from premadeCommentsToPublish array.
												var commentToPublish = premadeCommentsToPublish.shift();

												console.log("Publishing premade comment for confession #" + confession._id);

												premadeContentController.publishPremadeComment(commentToPublish);

												// Stop the cron job since we do not need it anymore.
												this.stop();
												// Remove the cron job from postPremadeCommentsJobs array.
												postPremadeCommentsJobs.shift();
											},
											start: false,
											timeZone: 'UTC'
										});

										// Push the newly created cron job to postPremadeCommentsJobs array.
										postPremadeCommentsJobs.push(publishPremadeComment);

										// Push related confession object to confessionsToPublish array.
										premadeCommentsToPublish.push(comment);
									}
								}

								// Once all the cron jobs have been created and pushed to postPremadeCommentsJobs array, we can start them.
								console.log("Starting publish premade comments cron jobs...");
								for (var k = 0; k < postPremadeCommentsJobs.length; k++) {
									postPremadeCommentsJobs[k].start();
								}

							}
						}); 
					});

					// Stop the cron job since we do not need it anymore.
					this.stop();

					// Remove the cron job from postPremadeConfessionsJobs array.
					postPremadeConfessionsJobs.shift();

				},
				start: false,
				timeZone: 'UTC'
			});

			// Push the newly created cron job to postPremadeConfessionsJobs array.
			postPremadeConfessionsJobs.push(postPremadeJob);

			// Push related confession object to premadeConfessionsToPublish array.
			premadeConfessionsToPublish.push(premade_confession);

			// Turn on scheduled cron job.
			postPremadeConfessionsJobs[0].start();
		}

	});
}


var checkPremadeQueue = function() {

	// Get all entries.
	PremadeJobQueue.find({}, function(err, results) {

		for (var i = 0; i < results.length; i++) {
			var queue_entry = results[i];

			if (queue_entry.post_type == "confession") {

				console.log("Premade confession found in queue.");
				console.log(queue_entry);

				// This job should have already been executed. Execute it immediately.
				if (queue_entry.scheduled_timestamp <= moment.utc().toISOString()) {
					console.log("Publishing premade confession from queue immediately.");
					publishPremadeConfessionAndComments(queue_entry.premade_id);
				} 
				// Create a cron job to be executed in the future.
				else {

					console.log("Scheduling premade confession post job at " + queue_entry.scheduled_timestamp);

					premadeContentController.getPremadeConfession(queue_entry.premade_id, function(premade_confession) {

						var publish_confession_timestamp = moment.utc(queue_entry.scheduled_timestamp);

						var premadeConfessionCronTime = publish_confession_timestamp.seconds() + ' ' +
													publish_confession_timestamp.minutes() + ' ' +
													publish_confession_timestamp.hours() + ' ' +
													publish_confession_timestamp.date() + ' ' +
													publish_confession_timestamp.month() + ' *';

						// Create new cron job
						var jobPostPremadeConfession = new CronJob({
							cronTime: premadeConfessionCronTime,
							onTick: function() {

								// Get the first related confession object from premadeConfessionsToPublish array.
								var premade_confession_to_publish = premadeConfessionsToPublish.shift();

								// Publish the premade confession.
								publishPremadeConfessionAndComments(premade_confession_to_publish._id);

								// Stop the cron job since we do not need it anymore.
								this.stop();

								// Remove the cron job from postPremadeConfessionsJobs array.
								postPremadeConfessionsJobs.shift();

							},
							start: false,
							timeZone: 'UTC'
						});

						// Push the newly created cron job to postPremadeConfessionsJobs array.
						postPremadeConfessionsJobs.push(jobPostPremadeConfession);

						// Push related confession object to premadeConfessionsToPublish array.
						premadeConfessionsToPublish.push(premade_confession);

						// Turn on scheduled cron job.
						postPremadeConfessionsJobs[0].start();

					});
				}
			}
		}
	});
}


exports.popularityCheck = popularityCheck;
exports.publishToSocialMedia = publishToSocialMedia;
exports.shareOnInstagram = shareOnInstagram;
exports.postPremade = postPremade;
exports.checkPremadeQueue = checkPremadeQueue;