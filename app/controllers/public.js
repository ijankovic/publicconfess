// Load packages
var http = require('http');
var async = require('async');
var moment = require('moment');

// Load controllers
var confessionController = require('../controllers/confession');
var pendingConfessionController = require('../controllers/pending_confession');
var commentController = require('../controllers/comment');

// Get counter model
var Counter = require('../models/counter');



// Helper functions
// ––––––––––––––––––––––––––––––––––––––––––––––––––

function renderPagination(page, prev, next, current_page) {

	// page parameter is either new or popular.

	var html = '';
	var prev_page = current_page - 1;
	var next_page = current_page + 1;

	// Render previous page button only
	if (prev && !next) {
		html += '<a href="/' + page + '/' + prev_page + '"><div class="prev_page single_button">' +
					'<span class="icon icon-chevron-left"></span>' +
				'</div></a>';
	}

	// Render next page button only
	else if (!prev && next) {
		html += '<a href="/' + page + '/' + next_page + '"><div class="next_page single_button">' +
					'<span class="icon icon-chevron-right"></span>' +
				'</div></a>';
	}

	// Render both buttons
	else if (prev && next) {
		html += '<a href="/' + page + '/' + prev_page + '"><div class="prev_page">' +
					'<span class="icon icon-chevron-left"></span>' +
				'</div></a>';

		html += '<a href="/' + page + '/' + next_page + '"><div class="next_page">' +
					'<span class="icon icon-chevron-right"></span>' +
				'</div></a>';
	}

	return html;
}


function confessionDoesNotExistStatus(confession_id) {
	return '<h2>Confession <span class="id">#' + confession_id + '</span> does not exist.</h2>' +
			'<p>It could be because of the following reasons:</p>' +
			'<ul>' +
				'<li>Confession received a low rating in "Be Admin" section where visitors decide by voting which confessions get published.</li>' +
				'<li>Confession was removed due to breaking site rules.</li>' +
				'<li>Confession was never submitted.</li>' +
			'</ul>';
}

function pendingConfessionSuspiciousStatus(confession_id) {
	return '<h2>Confession <span class="id">#' + confession_id + '</span> is awaiting approval from our administration team.</h2>';
}

function pendingConfessionStatus(confession) {

	var total_votes = confession.yes + confession.no;
	var yes_percentage = Math.round( (confession.yes / total_votes) * 100 ) || 0;
	var no_percentage = (total_votes == 0 ? 0 : 100 - yes_percentage);

	return '<h2>Confession <span class="id">#' + confession._id + '</span> is pending.</h2>' +
			'<div class="pending_confession_preivew">' +
				'<div class="confession_header">' +
					'<span class="confession_id">#' + confession._id + '</span>' +
					'<span class="confession_time" timestamp="' + confession.timestamp + '"></span>' +
				'</div>' +
				'<p>' + confession.content + '</p>' +
			'</div>' +
			'<p>This confession is in "Be Admin" section where visitors decide by voting which confessions get published.</p>' +
			'<p>It will be published if it reaches 80% approval rate by the 20th vote or 50% approval rate by the 100th vote.</p>' +
			'<p>Here are some statistics on how visitors voted for confession <span class="id">#' + confession._id + '</span> so far:</p>' +

			'<table>' +
				'<tr><th colspan="2">Should this confession be published?</th></tr>' +
				'<tr>' +
					'<td>Yes</td>' +
					'<td>' + yes_percentage + '%</td>' +
				'</tr>' +
				'<tr>' +
					'<td>No</td>' +
					'<td>' + no_percentage + '%</td>' +
				'</tr>' +
				'<tr class="total_votes">' +
					'<td>Total Votes</td>' +
					'<td>' + total_votes + '</td>' +
				'</tr>' +
			'</table>';
}



// Export functions
// ––––––––––––––––––––––––––––––––––––––––––––––––––

var confessionsPage = function(page, page_num, callback) {

	var limit = 10; // how many confessions per page
	var offset = (page_num - 1) * limit;
	var sort;
	var sortorder = 'desc';

	
	if (page == 'new') {
		sort = 'time'; // On new page sort by time
	} else if (page == 'popular') {
		sort = 'popular'; // On popular page sort by popular
	}

	var view; // used to store data for mustache template
	var pagination_html; // used to store HTML for pagination

	// This variable is used to get one extra confession. If we are able to get extra confession, then
	// it means there is going to be extra page and we can render next page button.
	var temp_limit = limit + 1;

	confessionController.read_collection(temp_limit, offset, sort, sortorder, function(err, data) {

		if (err) {
			callback(err, null);
		} else {

			if (data.length == 0) {

				pagination_html = renderPagination(page, false, false, page_num);

			} else if (data.length > 0 && data.length <= 10) {

				if (page_num == 1) {

					pagination_html = renderPagination(page, false, false, page_num);
					
				} else {

					pagination_html = renderPagination(page, true, false, page_num);
				}
			} else if (data.length > 10) {

				if (page_num == 1) {
					// Pop extra confession since it is used only to determine if next page button should be rendered.
					data.pop();
					pagination_html = renderPagination(page, false, true, page_num);
				} else {
					// Pop extra confession since it is used only to determine if next page button should be rendered.
					data.pop();
					pagination_html = renderPagination(page, true, true, page_num);
				}
			}
		}

		view = { confessions: data, pagination: pagination_html };
		callback(err, view);
	});
}


var singleConfessionPage = function(confession_id, finalCallback) {

	var view; // used to store data for mustache template

	confessionController.read_resource(confession_id, function(err, publishedConfession) {

		if (err == 500) {
			// Server error, problem getting confession.
			finalCallback(err, null);
		} else if (err == 404) {
			// There is no confession with given confession_id.

			// Check if there is pending confession with given confession_id.
			pendingConfessionController.read_resource(confession_id, function(err2, pendingConfession) {

				if (err == 500) {
					// Server error, problem getting pending confession.
					finalCallback(err2, null);

				} else if (err2 == 404) {
					// There is no pending confession with given confesison_id.
					view = { status: confessionDoesNotExistStatus(confession_id)}
					finalCallback(null, view);

				} else if (pendingConfession.suspicious) {
					view = { status: pendingConfessionSuspiciousStatus(confession_id)}
					finalCallback(null, view);
				} else {
					// Pending confession.
					view = { status: pendingConfessionStatus(pendingConfession)}
					finalCallback(null, view);
				}
			});

		} else {
			// Published confession.

			// Get comments.
			commentController.read_collection(confession_id, 'confession', null, null, 'time', 'asc', function(err3, comments_data) {

				if (err3) {
					finalCallback(err3, null);
				} else {
					view = { confession: publishedConfession, comments: comments_data}
					finalCallback(null, view);
				}
			});
		}
	});
}



exports.confessionsPage = confessionsPage;
exports.singleConfessionPage = singleConfessionPage;