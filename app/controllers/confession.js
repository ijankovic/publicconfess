// Load internal packages
var utils = require('../utils');

// Get models
var PendingConfession = require('../models/pending_confession.js');
var Confession = require('../models/confession.js');

// Load controllers
var trendController = require('../controllers/trend');



// Get collection of confessions
var read_collection = function(limit, offset, sortby, sortorder, callback) {

	// Parameters used for callback
	var error = null;

	// Check if parameters are valid

	// Check offset parameter
	if (offset < 0) {
		error = 400;	// http error code
		console.log('This is not a valid offset parameter: ' + offset);
	}

	// Check sortorder parameter
	if (sortorder != "asc" && sortorder != "desc") {
		error = 400;	// http error code
		console.log('This is not a valid sortorder parameter: ' + sortorder);
	}

	var query = {};		// used to search only for confessions that have popular_flag set to true.
	var sort_query;

	// {newest, popular, ?trending, approve, condemn, num_comments, fb_likes, tweets} 

	/*
			*popular = sort by total number of likes + dislikes + fb_likes + tweets + comments
			?trending = confessions that have greatest jump in popularity number (sum of all values) for the past hour. This list would be presorted by cron job every hour.

		  from     (optional, default = Monday of the current week)
		  to	   (optional, default = Sunday of the current week)

	*/

	// Check sortby parameter
	if (sortby == 'time') {
		sort_query = {'timestamp': sortorder}
	} else if (sortby == 'popular') {
		// Some confessions are flaged as popular. Those confessions would have their popular flag set to true.
		// When user requests sortby popular, we need to return only confessions that have their popular flag set to true. 
		query = {'popular_flag': true};
		sort_query = {'timestamp': sortorder};

	} else if (sortby == 'trending') {

		//sort_query = {'xxxxx': sortorder}


		// THIS IS MORE COMPLEX TO IMPLEMENT

	} else if (sortby == 'approve') {
		sort_query = {'approve': sortorder}
	} else if (sortby == 'condemn') {
		sort_query = {'condemn': sortorder}
	} else if (sortby == 'num_comments') {
		sort_query = {'num_comments': sortorder}
	} else if (sortby == 'fb_likes') {
		sort_query = {'fb_likes': sortorder}
	} else if (sortby == 'tweets') {
		sort_query = {'tweets': sortorder}
	} else {
		error = 400;	// http error code
		console.log('This is not a valid sortby parameter: ' + sortby);
	}


	if (error) {
		callback(error, null);
	} else {

		Confession.find(query)
						.select()
						.limit(limit)
						.skip(offset)
						.sort(sort_query)
						.exec(function(err, confessions) {
							if (err) {
								error = 500;	// http error code
								console.log("Error trying to read confessions in read_collection function in controllers/confessions: " + err);
							}

							callback(error, confessions);
						});
	}
}



// Get confession
var read_resource = function(id, callback) {

	// Parameters used for callback
	var error = null;

	Confession.findById(id, function(err, confession) {
		if (err) {
			error = 500;	// http error code
			console.log("Error trying to read confession in read_resource in controllers/confession: " + err);
		}

		if (confession == null) {
			error = 404;	// http error code
			// console.log("Trying to get confession with invalid id.");
		}
		
		callback(error, confession);
	});
}



// Update approve, condemn, fb_like, tweet, comment field in confession
var update = function(id, action, callback) {

	var action_timestamp = new Date().toISOString();

	// Parameters used for callback
	var error = null;

	Confession.findById(id, function(err, confession) {
		if (err) {
			error = 500;	// http error code
			console.log("Error trying to update confession in update function in controllers/confession: " + err);
		}

		if (confession == null) {
			error = 404;	// http error code
			console.log("Trying to update confession with invalid id.");
			callback(error, null);
		} else {

			// Update parameter
			switch(action) {
				case 'approve':
					confession.approve = confession.approve + 1;
					break;
				case 'condemn':
					confession.condemn = confession.condemn + 1;
					break;
				case 'fb_like':
					confession.fb_like = confession.fb_like + 1;
					break;
				case 'tweet':
					confession.tweet = confession.tweet + 1;
					break;
				case 'comment':
					confession.num_comments = confession.num_comments + 1;
					break;
				case 'popular':
					confession.popular_flag = true;
					break;
				default:
					// action is invalid since it is not matching approve, condemn, fb_like, tweet, or comment
					error = 400;
					console.log("Trying to update confession with invalid action parameter.");
					callback(error, confession);
			}

			confession.popularity += 1;

			// Check if actions should be logged into trends table for popularity calculation.
			trendController.add(confession, action_timestamp);

			confession.save(function(err) {
				if (err) {
					error = 500;	// http error code
					console.log("Error trying to update confession in update function in controllers/confession: " + err);
				}

				callback(error, confession);
			});
		}
	});
}


// Remove confession
var remove = function(id, callback) {

	// Parameters used for callback
	var error = null;

	Confession.findByIdAndRemove(id, function(err) {
		if (err) {
			error = 500;	// http error code
			console.log("Error trying to delete confession in delete function in controllers/confession: " + err);
		}
		
		callback(error);
	});
}


var setPopularFlag = function(id) {

	console.log("Labeling confession #" + id + " as popular.");

	Confession.findByIdAndUpdate(id, { popular_flag: 'True' }, function(err, confession) {
		if (err) {
			console.log("Error in setPopularFlag function in controllers/confession: " + err);
		}
	});
}



exports.read_resource = read_resource;
exports.read_collection = read_collection;
exports.update = update;
exports.remove = remove;
exports.setPopularFlag = setPopularFlag;