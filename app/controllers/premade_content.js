// Load external packages
var lineReader = require('line-reader');

// Load internal packages
var PremadeConfession = require('../models/premade_confession');
var PendingConfession = require('../models/pending_confession');
var Confession = require('../models/confession');
var commentController = require('../controllers/comment');
var trendController = require('../controllers/trend');
var utils = require('../utils');


// Global variables
// ===================================================
var pathToFile = "./app/data/premade_content.tsv";
var latest_premade_confession_id;


// Helper functions
// ===================================================


function processNextLine(reader) {

	if (reader.hasNextLine()) {

		reader.nextLine(function(line) {

			var chunks = line.split("\t");

			// Confession line
			if (chunks[1] == "conf") {

				var premade_confession_id = chunks[0]
				var premade_confession_content = chunks[3];

				latest_premade_confession_id = premade_confession_id;

				console.log(premade_confession_id + " | " + premade_confession_content + "\n");

				// Create a new instance of the PremadeConfession model
				var premadeConfession = new PremadeConfession();

				premadeConfession._id = premade_confession_id;
				premadeConfession.content = premade_confession_content;

				// Save the pendingConfession and check for errors
				premadeConfession.save(function(err) {
					if (err) {
						console.log("Error trying to save pending confession in create function in controllers/pending_confession: " + err);
					}

					processNextLine(reader);
				});
			}
			// Comment line
			else if (chunks[1].indexOf("com") > -1) {

				var premade_comment_content = chunks[3];
				
				PremadeConfession.findById(latest_premade_confession_id, function(err, premade_confession) {
					if (err) {
						console.log("Error in premade_content.processNextLine: " + err);
					}

					premade_confession.comments.push(premade_comment_content);

					premade_confession.save(function(err) {
						if (err) {
							console.log("Error in premade_content.processNextLine: " + err);
						}

						processNextLine(reader);
					});
				});
			}
			// Empty line
			else {
				processNextLine(reader);
			}
		});
	}
}


function getRandomCommentAuthor() {
	var authors = ["Anonymous", "Mark", "David", "Sara", "Julian", "Alexandra", "Hailey", "Matthew", "Evan", "Hannah", "Benjamin", "Adam", "Kyla", "Olivia",
					"Emma", "Alice", "Robert", "Lucas", "Kate", "Jordan", "Trevor", "Scott", "Bonny", "Nicholas", "Brennan", "Oscar", "Madeline", "Dominic",
					"Dimitri", "Ian", "Victoria", "Eliot", "Claire", "Eric", "Isabelle", "Chelsea", "Natalie", "Stefan", "Gabriel", "Jessica", "Anna",
					"Dennis", "Yulia", "Peter", "Karl", "Ella", "Thomas", "Zoe", "Lucy", "Sydney", "Vicky", "Katherine", "Tamara", "Rachel", "Bianca",
					"Alex", "Niki", "Duncan", "Greg", "Courtney", "Chris", "Bela", "Ieva", "Lauren", "Samantha", "Rina", "Veronica", "Igor", "Ivan"];

	var randomAuthorIndex = utils.getRandomInt(0, authors.length - 1);

	return authors[randomAuthorIndex];
}


// Export functions
// ===================================================


var parsePremadeConfessions = function() {

	lineReader.open(pathToFile, function(reader) {
		processNextLine(reader);
	});
}


var getNextPremadeConfession = function(callback) {

	utils.getNextId("premade_confession_id", function(next_id) {
		console.log("Next id for premade confession is: " + next_id);

		PremadeConfession.findById(next_id, function(err, premade_confession) {
			if (err) {
				console.log("Error in premade_content.getNextPremadeConfession: " + err);
			} else {
				callback(premade_confession);
			}
		});
	});
}


var getPremadeConfession = function(premade_id, callback) {
	PremadeConfession.findById(premade_id, function(err, premade_confession) {
		if (err) {
			console.log("Error in premade_content.getNextPremadeConfession: " + err);
		} else {
			callback(premade_confession);
		}
	});
}


var publishPremadeConfession = function(premade_confession, callback) {

	// Get random actions numbers.
	var randApprove = utils.getRandomInt(100, 200);
	var randCondemn = utils.getRandomInt(5, 30);
	var randFacebook = utils.getRandomInt(10, 20);
	var randTweet = utils.getRandomInt(0, 15);
	var randPopularity = randApprove + randCondemn + randFacebook + randTweet; // minimum possible premade popularity could be 220.

	// Create a new instance of the Confession model
	var confession = new Confession();

	// call helper function to get next available confession id
	utils.getNextId("confession_id", function(next_id) {
		confession._id = next_id;
		confession.content = premade_confession.content;
		confession.approve = randApprove;
		confession.condemn = randCondemn;
		confession.num_comments = 0;
		confession.fb_like = randFacebook;
		confession.tweet = randTweet;
		confession.popularity = randPopularity;
		confession.popular_flag = false;
		confession.timestamp = new Date().toISOString();			// returns string like "2015-01-04T05:38:23.881Z"

		// Update real_id in premade_confession collection with actual confession id.
		// We need this information for later when posting comments.
		PremadeConfession.findById(premade_confession._id, function(err, premade_confession_result) {
			if (err) {
				console.log("Error trying to retrieve premade confession in publishPremadeConfession function in controllers/premade_content: " + err);
			} else {
				premade_confession_result.real_id = confession._id;
				premade_confession_result.save(function (err) {
					if (err) {
						console.log("Error trying to save premade_confession_result in publishPremadeConfession function in controllers/premade_content: " + err);
					}
				});
			}
		});

		// Save the pendingConfession and check for errors
		confession.save(function(err) {
			if (err) {
				console.log("Error trying to save confession in publishPremadeConfession function in controllers/premade_content: " + err);
			}

			// Create new trend record for the newly published confession where new trend count is set to precalculated popularity.
			trendController.init(confession, confession.popularity);

			console.log('New premade confession published. { _id: ' + confession._id + ', content: "' + confession.content + '" }');

			callback(confession);
		});
	});
}



var getPremadeComments = function(premade_confession_id, callback) {

	PremadeConfession.findById(premade_confession_id, function(err, premade_confession_result) {
		if (err) {
			console.log("Error trying to retrieve premade confession in getPremadeComments function in controllers/premade_content: " + err);
		} else {
			callback(premade_confession_result.comments);
		}
	});

}


var publishPremadeComment = function(comment) {

	var author = getRandomCommentAuthor();

	commentController.create(comment.content, comment.reply_to_id, "confession", author, function(err, comment_result) {
		if (err) {
			console.log("Error trying to post premade comment for confession #" + comment.reply_to_id + ": " + err);
		} else {
			console.log("Premade comment #" + comment_result._id + " for confession #" + comment.reply_to_id + " has been successfully published.");
		}
	});
}


exports.parsePremadeConfessions = parsePremadeConfessions;
exports.getNextPremadeConfession = getNextPremadeConfession;
exports.getPremadeConfession = getPremadeConfession;
exports.publishPremadeConfession = publishPremadeConfession;
exports.getPremadeComments = getPremadeComments;
exports.publishPremadeComment = publishPremadeComment