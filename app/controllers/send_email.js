var sendgrid = require("sendgrid")("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");

var email = new sendgrid.Email();

var sendEmail = function(sender_name, sender_email, message_body, form_name, callback) {

	var message = "<p>" + sender_name + " - " + sender_email + "</p>" + message_body;

	console.log(message);

	var email = new sendgrid.Email({
		to: 'publicconfesscom@gmail.com',
		from: 'form.submit@publicconfess.com',
		fromname: 'PublicConfess.com',
		subject: "New email from PublicConfess.com - " + form_name,
		html: message
	});

	sendgrid.send(email, function(err, json) {
		if (err) {
			console.log("Error trying to send email in send_email controller: " + err);
			callback(err);
		} else {
			console.log("Email sent successfully.");
			callback();
		}
	});
}


exports.sendEmail = sendEmail;