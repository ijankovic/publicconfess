// Load internal packages
var utils = require('../utils');

// Get models
var PendingConfession = require('../models/pending_confession.js');
var Confession = require('../models/confession.js');

// Load controllers
var trendController = require('../controllers/trend');


// Helper functions

function readyToPublish(confession, callback) {

	var MIN_VOTES = 20;
	var MAX_VOTES = 100;
	var YES_VOTE_RATE_EARLY = 0.8;
	var YES_VOTE_RATE_LATE = 0.5;
	var total_votes = confession.yes + confession.no;
	var yes_votes_percentage;

	// If there are at least MIN_VOTES votes, but less than MAX_VOTES, approve if there are more than YES_VOTE_RATE_EARLY percentage of "Yes" votes.
	if (total_votes >= MIN_VOTES && total_votes < MAX_VOTES) {

		yes_votes_percentage = Math.round( (confession.yes / total_votes) * 10000 ) / 10000;  // round to 4 decimal places

		if (yes_votes_percentage >= YES_VOTE_RATE_EARLY) {
			approve(confession._id, function() {
				callback();
			});
		} else {
			callback();
		}
	}
	// If there are more than MAX_VOTES, approve if there are more than YES_VOTE_RATE_LATE percentage of "Yes" votes.
	else if (total_votes >= MAX_VOTES) {
		yes_votes_percentage = Math.round( (confession.yes / total_votes) * 10000 ) / 10000;  // round to 4 decimal places

		if (yes_votes_percentage >= YES_VOTE_RATE_LATE) {
			approve(confession._id, function() {
				callback();
			});
		} else {
			//remove(confession._id, function() {
				callback();
			//});
		}
	}
}



// Create pending confession
var create = function(content, callback) {

	// Parameters used for callback
	var error = null;

	// check for possible attack
	//if (utils.checkContent(content) != "") {
	// if () {
	// 	//content is not safe
	// 	error = 400;	// http error code
	// 	//console.log("Content seems suspicious.");
	// 	//Call callback function immediately and do not continue with saving.
	// 	callback(error, null);		// data is null.
		
	// } else {

		// Create a new instance of the Pending Confession model
		var pendingConfession = new PendingConfession();

		// call helper function to get next available confession id
		utils.getNextId("confession_id", function(next_id) {
			pendingConfession._id = next_id;
			pendingConfession.content = content;
			pendingConfession.yes = 0;
			pendingConfession.no = 0;
			pendingConfession.report = 0;
			pendingConfession.suspicious = utils.checkContent(content);		// true if content seems to be suspicious
			pendingConfession.timestamp = new Date().toISOString();			// returns string like "2015-01-04T05:38:23.881Z"

			// Save the pendingConfession and check for errors
			pendingConfession.save(function(err) {
				if (err) {
					error = 500;	// http error code
					console.log("Error trying to save pending confession in create function in controllers/pending_confession: " + err);
				}

				// When everything is finished call callback function provided which needs two parameters: error, data
				callback(error, pendingConfession);
			});
		});
	// }
}


// Get collection of pending confessions
var read_collection = function(query, limit, offset, sortby, sortorder, callback) {

	// Parameters used for callback
	var error = null;

	// Check if parameters are valid

	try {
		var query_operator = JSON.parse(query);
	} catch (e) {
		error = 400;
		console.log('This is not a valid query parameter: ' + query);
	}

	// Check limit parameter
	if (limit < 1 || limit > 20) {
		error = 400;	// http error code
		console.log('This is not a valid limit parameter: ' + limit);
	}

	// Check offset parameter
	if (offset < 0) {
		error = 400;	// http error code
		console.log('This is not a valid offset parameter: ' + offset);
	}

	// Check sortorder parameter
	if (sortorder != "asc" && sortorder != "desc") {
		error = 400;	// http error code
		console.log('This is not a valid sortorder parameter: ' + sortorder);
	}

	var sort_query;

	// Check sortby parameter
	if (sortby == 'time') {
		sort_query = {'timestamp': sortorder}
	} else if (sortby == 'yes') {
		sort_query = {'yes': sortorder}
	} else if (sortby == 'no') {
		sort_query = {'no': sortorder}
	} else if (sortby == 'report') {
		sort_query = {'report': sortorder}
	} else {
		error = 400;	// http error code
		console.log('This is not a valid sortby parameter: ' + sortby);
	}

	if (error) {
		callback(error, null);
	} else {

		PendingConfession.find(query_operator)
						.select()
						.limit(limit)
						.skip(offset)
						.sort(sort_query)
						.exec(function(err, confessions) {
							if (err) {
								error = 500;	// http error code
								console.log("Error trying to read pending confessions in read_collection function in controllers/pending_confessions: " + err);
							}

							callback(error, confessions);
						});
	}
}


// Get pending confession
var read_resource = function(id, callback) {

	// Parameters used for callback
	var error = null;

	PendingConfession.findById(id, function(err, confession) {
		if (err) {
			error = 500;	// http error code
			console.log("Error trying to read pending confession in read_resource in controllers/pending_confession: " + err);
		}

		if (confession == null) {
			error = 404;	// http error code
			// console.log("Trying to get pending confession with invalid id.");
		}
		
		callback(error, confession);
	});
}

// Update yes, no or report field in pending confession
var update = function(id, action, callback) {

	// Parameters used for callback
	var error = null;

	PendingConfession.findById(id, function(err, confession) {
		if (err) {
			error = 500;	// http error code
			console.log("Error trying to update pending confession in update function in controllers/pending_confession: " + err);
		}

		if (confession == null) {
			error = 404;	// http error code
			console.log("Trying to update pending confession with invalid id.");
			callback(error, null);
		} else {

			// Update parameter
			switch(action) {
				case 'yes':
					confession.yes = confession.yes + 1;
					break;
				case 'no':
					confession.no = confession.no + 1;
					break;
				case 'report':
					confession.report = confession.report + 1;
					break;
				default:
					// action is invalid since it is not matching yes, no or report
					error = 400;
					console.log("Trying to update pending confession with invalid action parameter.");
			}

			confession.save(function(err) {
				if (err) {
					error = 500;	// http error code
					console.log("Error trying to update pending confession in update function in controllers/pending_confession: " + err);
				}

				// Checking if confession is ready to be published should not be blocking.
				readyToPublish(confession, function() {});

				callback(error, confession);
			});
		}
	});
}


// Remove pending confession
var remove = function(id, callback) {

	// Parameters used for callback
	var error = null;

	PendingConfession.findByIdAndRemove(id, function(err) {
		if (err) {
			error = 500;	// http error code
			console.log("Error trying to delete pending confession in delete function in controllers/pending_confession: " + err);
		}
		
		callback(error);
	});
}


// Approve pending confession - Move from pending_confessions collection to confessions collection
var approve = function(id, callback) {

	// Parameters used for callback
	var error = null;

	PendingConfession.findById(id, function(err, pendingConfession) {
		if (err) {
			error = 500;	// http error code
			console.log("Error trying to approve pending confession in approve function in controllers/pending_confession: " + err);
		}

		if (pendingConfession == null) {
			error = 404;	// http error code
			console.log("Trying to approve pending confession with invalid id.");
			callback(error, null);
		} else {

			// Create a new instance of the Confession model
			var confession = new Confession();

			confession._id = pendingConfession._id;						// confession will have the same id as pending confession
			confession.content = pendingConfession.content;				// confession will have the same content as pending confession
			confession.approve = 0;
			confession.condemn = 0;
			confession.num_comments = 0;
			confession.fb_like = 0;
			confession.tweet = 0;
			confession.popularity = 0;
			confession.popular_flag = false;
			confession.timestamp = new Date().toISOString();			// returns string like "2015-01-04T05:38:23.881Z"

			// Save the confession and check for errors
			confession.save(function(err) {
				if (err) {
					error = 500;	// http error code
					console.log("Error trying to save confession in approve function in controllers/pending_confession: " + err);
				} else {
					// Delete confession from pending_confessions collection once it is saved to confessions collection
					remove(pendingConfession._id, function(err) {
						if (err) {
							error = 500;
							console.log("Error trying to delete pending confession in approve function in controllers/pending_confession: " + err);
						} else {

							// Create new trend record for the newly published confession.
							trendController.init(confession);

							console.log('New confession published. { _id: ' + confession._id + ', content: "' + confession.content + '" }');

							// Increase approved confessions counter by 1
							utils.increaseCount("approved_confessions", function(count) {

								// When everything is finished call callback function provided which needs two parameters: error, data
								callback(error, confession);
							});
						}
					});
				}
			});
		}
	});
}

exports.create = create;
exports.read_collection = read_collection;
exports.read_resource = read_resource;
exports.update = update;
exports.remove = remove;
exports.approve = approve;