var moment = require('moment');

// Get models
var Trend = require('../models/trend.js');


// Global variables

// Use environment defined trend period or default to 3 hours.
var trendPeriod = process.env.TREND_PERIOD || 24;

// Use environment defined popularity percentile or default to 0.6
var popularityPercentile = process.env.POPULARITY_PERCENTILE || 0.6;

var trendHistory = [];



// Helper functions

// Used to check if confession has been published within trendPeriod time of the vote (action).
function checkRecentTrend(confession_timestamp, action_timestamp) {
	var trend_time = moment.utc(action_timestamp);
	var confession_time = moment.utc(confession_timestamp);

	var time_difference = trend_time.diff(confession_time, 'hours');

	if (time_difference < trendPeriod) {
		// Action is within trendPeriod timeframe. Count this action.
		return true;
	} else {
		// Action is outside of trendPeriod timeframe. Do not count this action.
		return false;
	}
}

// Used to sort trend documents by trend count.
function compareByTrend(a, b) {
	if (a.trend < b.trend)
		return 1;
	if (a.trend > b.trend)
		return -1;
	return 0;
}

// Used to sort trend documents by timestamp.
function compareByTimestamp(a, b) {
	if (a.timestamp < b.timestamp)
		return -1;
	if (a.timestamp > b.timestamp)
		return 1;
	return 0;
}

// Finds percentile
function findPercentile(trend_count) {
	var rank = trend_count * popularityPercentile;

	var rank_remainder = rank % 1;

	// Check if rank has decimals.
	if (rank_remainder != 0) {

		// Rank has decimals.
		if (rank_remainder >= 0.5) {
			// Round up.
			return Math.ceil(rank);
		} else {
			// Round down.
			return Math.floor(rank);
		}

	} else {
		// Rank does not have decimals.
		return rank;
	}
}

function getMovingMedian() {

	var median = 0;

	// trendHistory array will always have 3 or less elements.
	// The following cases based on the number of elements inside trendHistory, set the median.
	switch (trendHistory.length) {
		case 0:
			median = 0;
			break;
		case 1:
			median = trendHistory[0];
			break;
		case 2:
			median = (trendHistory[0] + trendHistory[1]) / 2;
			break;
		case 3:
			median = trendHistory[1];
			break;
	}

	return median;
}

function updateTrendHistory(new_trend) {
	
	trendHistory.push(new_trend);

	// Trend History is set to store 3 most recent trends.
	if (trendHistory.length > 3) {
		trendHistory.shift();
	}

	trendHistory.sort( function(a,b) { return a - b; } );
}


// Create new trend record for the confession. This function gets called right before confession gets published.
var init = function(confession, trend_init_count) {

	// If trend_init_count parameter is not passed, we will set it to 0.
	trend_init_count = typeof trend_init_count !== 'undefined' ? trend_init_count : 0;

	var new_trend = new Trend();

	new_trend._id = confession._id;
	new_trend.timestamp = confession.timestamp;
	new_trend.trend = trend_init_count;

	new_trend.save(function(err) {
		if (err) {
			console.log("Error trying to save trend in trendController.init: " + err2);
		}
	});
}



// Add new trend entry.
var add = function(confession, action_timestamp) {


	// Check if trend is recent, within trendPeriod
	if (checkRecentTrend(confession.timestamp, action_timestamp)) {

		// Increase trend count
		Trend.findByIdAndUpdate(confession._id, {$inc: {trend:1}}, function(err) {
			if (err) {
				console.log("Error trying to update trend in trendController.add: " + err);
			}
		});
	}
}

// Flags popular confessions within given trend time range.
var getPopular = function(trend_start_timestamp, trend_end_timestamp, callback) {

	// Find all confessions published between trend_start_timestamp and trend_end_timestamp and
	// select _id and votes fields.
	Trend.find({timestamp: {$gte: trend_start_timestamp, $lt: trend_end_timestamp}}, function (err, results) {

		if (err) {
			console.log("Error trying to retrieve trends in trendController.getPopular: " + err);
			callback(null);		// return no results
		} else {

			results.sort(compareByTrend);

			console.log(results.length + " confessions found for trend analysis:");
			console.log(results);

			console.log("Popularity percentile: " + popularityPercentile * 100 + "%");
			// Find how many items are in the percentile.
			var percentile = findPercentile(results.length);
			console.log(percentile + " top confessions are within popularity percentile.");

			// Make sure there are confessions in the percentile.
			if (percentile > 0) {

				var lowestAcceptableTrend = results[percentile - 1].trend;
				console.log("Lowest Acceptable Trend: " + lowestAcceptableTrend);

				var trend_median = getMovingMedian();
				console.log("Trend Median: " + trend_median);

				updateTrendHistory(lowestAcceptableTrend);
				console.log("trendHistory after update: " + trendHistory);

				var popularResults;

				// Check if trend_median is lower than lowestAcceptableTrend.
				// if ((trend_median < lowestAcceptableTrend) && (trend_median != 0)) {
				if (trend_median < lowestAcceptableTrend) {
					// Filter results greater than or equal to trend_median.
					popularResults = results.filter(function(x) { return x.trend >= trend_median; });
				} else {
					popularResults = results.filter(function(x) { return x.trend >= lowestAcceptableTrend; });
				}

				popularResults.sort(compareByTimestamp);
				console.log(popularResults.length + " confession(s) will be flaged as popular.");
				console.log("Popular Results: " + popularResults);

				callback(popularResults);
			} else {

				// No results to pass.
				callback(null); 
			}
		}

	});
}


// Return trend of the most popular confession within the given timeframe.
var getMostPopular = function(from_timestamp, to_timestamp, callback) {

	Trend.find({timestamp: {$gte: from_timestamp, $lt: to_timestamp}})
		.sort({ trend: 'desc' })
		.limit(1)
		.exec(function(err, results) {
			if (err) {
				console.log("Error trying to retrieve trends in trendController.getMostPopular: " + err);
				callback(null);		// return no results
			} else {
				console.log(results);
				callback(results);
			}
		});
}


// Remove confession
// var remove = function(id, callback) {

// 	// Parameters used for callback
// 	var error = null;

// 	Confession.findByIdAndRemove(id, function(err) {
// 		if (err) {
// 			error = 500;	// http error code
// 			console.log("Error trying to delete confession in delete function in controllers/confession: " + err);
// 		}
		
// 		callback(error);
// 	});
// }


exports.init = init;
exports.add = add;
exports.getPopular = getPopular;
exports.getMostPopular = getMostPopular;