// Load internal packages
var utils = require('../utils');
var confessionController = require('../controllers/confession');

// Get models
var Comment = require('../models/comment.js');


// Create comment
var create = function(content, reply_to_id, reply_to, author, callback) {

	// Parameters used for callback
	var error = null;

	// // check for possible attack
	// if (utils.checkForAttack(content)) {

	// 	// content is not safe
	// 	error = 400;	// http error code
	// 	console.log("Content seems suspicious.");
	// 	// Call callback function immediately and do not continue with saving.
	// 	callback(error, null);		// data is null.
		
	// } 
	// // check for inappropriate words in comment content or author name
	// else if (utils.checkIfInappropriate(content) || utils.checkIfInappropriate(author)) {

	// 	// content or author name is inappropriate
	// 	error = 400;	// http error code
	// 	console.log("Content or author name is inappropriate.");
	// 	// Call callback function immediately and do not continue with saving.
	// 	callback(error, null);
		
	// } 
	// // comment is safe and appropriate
	// else {

		// Create a new instance of the Comment model
		var comment = new Comment();

		// call helper function to get next available comment id
		utils.getNextId("comment_id", function(next_id) {
			comment._id = next_id;
			comment.reply_to_id = reply_to_id;
			comment.reply_to = reply_to;
			comment.content = content;
			comment.author = author;
			comment.suspicious = utils.checkContent(content);		// true if content seems to be suspicious
			comment.likes = 0;
			comment.dislikes = 0;
			comment.report = 0;
			comment.timestamp = new Date().toISOString();		// returns string like "2015-01-04T05:38:23.881Z"

			// Save the comment and check for errors
			comment.save(function(err) {
				if (err) {
					error = 500;	// http error code
					console.log("Error trying to save comment in create function in controllers/comment: " + err);

					// When everything is finished call callback function provided which needs two parameters: error, data
					callback(error, null);
				} else {

					if (reply_to = "confession") {
						confessionController.update(reply_to_id, "comment", function(err2, confession) {

							if (comment.suspicious) {
								var info = JSON.parse(JSON.stringify(comment));
								info.message = "Your comment will be published once approved by our administration team.";
								callback(error, info);
							} else {
								// When everything is finished call callback function provided which needs two parameters: error, data
								callback(error, comment);
							}
							
						});
					} else {
						// It will go here only if reply_to is "comment"

						// When everything is finished call callback function provided which needs two parameters: error, data
						callback(error, comment);
					}
				}
			});
		});
	// }
}


// Get comments
var read_collection = function(reply_to_id, reply_to, limit, offset, sortby, sortorder, callback) {

	// Parameters used for callback
	var error = null;

	// Used to query comments for specfic confession or comment
	var query = {'reply_to_id': reply_to_id, 'reply_to': reply_to, 'suspicious': false};

	// Convert parameters from string to int
	offset = parseInt(offset);
	limit = parseInt(limit);

	// Check offset parameter
	if (offset < 0) {
		error = 400;	// http error code
		console.log('This is not a valid offset parameter: ' + offset);
	}

	// Check sortorder parameter
	if (sortorder != "asc" && sortorder != "desc") {
		error = 400;	// http error code
		console.log('This is not a valid sortorder parameter: ' + sortorder);
	}

	var sort_query;

	// Check sortby parameter
	if (sortby == 'time') {
		sort_query = {'timestamp': sortorder}
	} else if (sortby == 'likes') {
		sort_query = {'likes': sortorder}
	} else if (sortby == 'dislikes') {
		sort_query = {'dislikes': sortorder}
	} else if (sortby == 'report') {
		sort_query = {'report': sortorder}
	} else {
		error = 400;	// http error code
		console.log('This is not a valid sortby parameter: ' + sortby);
	}

	if (error) {
		callback(error, null);
	} else {

		Comment.find(query)
						.select()
						.limit(limit)
						.skip(offset)
						.sort(sort_query)
						.exec(function(err, comments) {
							if (err) {
								error = 500;	// http error code
								console.log("Error trying to read comments in read_collection function in controllers/comment: " + err);
							}

							callback(error, comments);
						});
	}
}


// NOT SURE IF YOU NEED A METHOD TO GET SINGLE COMMENT

// Get comment
// var read_resource = function(id, callback) {

// 	// Parameters used for callback
// 	var error = null;

// 	// include all properties except for '__v'
// 	PendingConfession.findById(id, '-__v', function(err, confession) {
// 		if (err) {
// 			error = 500;	// http error code
// 			console.log("Error trying to read pending confession in read_resource in controllers/pending_confession: " + err);
// 		}

// 		if (confession == null) {
// 			error = 404;	// http error code
// 			console.log("Trying to get pending confession with invalid id.");
// 		}
		
// 		callback(error, confession);
// 	});
// }


// Update like, dislike or report field in comment
var update = function(id, action, callback) {

	// Parameters used for callback
	var error = null;

	Comment.findById(id, function(err, comment) {
		if (err) {
			error = 500;	// http error code
			console.log("Error trying to update comment in update function in controllers/comment: " + err);
		}

		if (comment == null) {
			error = 404;	// http error code
			console.log("Trying to update comment with invalid id.");
			callback(error, null);
		} else {

			// Update parameter
			switch(action) {
				case 'like':
					comment.likes = comment.likes + 1;
					break;
				case 'dislike':
					comment.dislikes = comment.dislikes + 1;
					break;
				case 'report':
					comment.report = comment.report + 1;
					break;
				default:
					// action is invalid since it is not matching like, dislike or report
					error = 400;
					console.log("Trying to update comment with invalid action parameter.");
			}

			comment.save(function(err) {
				if (err) {
					error = 500;	// http error code
					console.log("Error trying to update comment in update function in controllers/comment: " + err);
				}

				callback(error, comment);
			});
		}
	});
}


// Remove comment
var remove = function(id, callback) {

	// Parameters used for callback
	var error = null;

	Comment.findByIdAndRemove(id, function(err) {
		if (err) {
			error = 500;	// http error code
			console.log("Error trying to delete comment in delete function in controllers/comment: " + err);
		}
		
		callback(error);
	});
}


exports.create = create;
exports.read_collection = read_collection;
//exports.read_resource = read_resource;
exports.update = update;
exports.remove = remove;