module.exports = function(api_router){



	// Load internal packages
	var pendingConfessionController = require('../controllers/pending_confession');



	// ---------------------------------------------------------------
	//					/api/pendingconfessions/
	// ---------------------------------------------------------------



	// Create endpoint GET: api/pendingconfession
	api_router.get('/pendingconfessions', function(req, res) {

		// Extract GET variables
		// All parameters are optional

		var query = (req.param('query')) ? req.param('query') : null;		// default query is null, return all documents
		var limit = (req.param('limit')) ? req.param('limit') : 10;			// default limit is 10
		var offset = (req.param('offset')) ? req.param('offset') : 0;			// default offset is 0
		var sortby = (req.param('sortby')) ? req.param('sortby') : "time";		// default sorting is by time
		var sortorder = (req.param('sortorder')) ? req.param('sortorder') : "desc";	// default sort order is descending

		pendingConfessionController.read_collection(query, limit, offset, sortby, sortorder, function(err, data) {

			// check for any errors
			if (err) {
				console.log('Error GET: api/pendingconfessions');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 400) {
					// Send invalid request code 400
					// The consumer gave bad data to the server and the server did nothing with it.
					res.status(400).json({ error: 'Invalid parameters. :('});
				}
			} else {
				// data will contain information about the newly created document in pending_confessions collection
				// 200 OK: The consumer requested data from the server, and the server found it for them.
				res.status(200).json(data);
			}
		});
	});



	// Create endpoint GET: api/pendingconfession/moderate
	api_router.get('/pendingconfessions/moderate', function(req, res) {

		var lastModeratedId = req.param('lastModeratedId');
		var query = '{"_id":{"$gt":' + lastModeratedId + '}, "suspicious":false}';
		var limit = 1;
		var offset = 0;
		var sortby = "time";
		var sortorder = "asc";

		pendingConfessionController.read_collection(query, limit, offset, sortby, sortorder, function(err, data) {

			// check for any errors
			if (err) {
				console.log('Error GET: api/pendingconfessions');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 400) {
					// Send invalid request code 400
					// The consumer gave bad data to the server and the server did nothing with it.
					res.status(400).json({ error: 'Invalid parameters. :('});
				}
			} else {
				// data will contain information about the newly created document in pending_confessions collection
				// 200 OK: The consumer requested data from the server, and the server found it for them.
				res.status(200).json(data);
			}
		});
	});



	// Create endpoint POST: api/pendingconfessions
	api_router.post('/pendingconfessions', function(req, res) {

		// Extract POST variables
		var content = req.body.content;

		pendingConfessionController.create(content, function(err, data) {

			// check for any errors
			if (err) {
				console.log('Error POST: api/pendingconfessions');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 400) {
					// Send invalid request code 400
					// The consumer gave bad data to the server and the server did nothing with it.
					res.status(400).json({ error: 'Invalid parameters. :('});
				}
			} else {
				// data will contain information about the newly created document in pending_confessions collection.
				console.log('New pending confession posted. { _id: ' + data._id + ', content: "' + data.content + '" }');

				// 201 CREATED: The consumer gave the server data, and the server created a resource.
				res.status(201).json(data);
			}
		});
	});



	// -----------------------------------------------------------------
	//			/api/pendingconfessions/:pendingconfession_id
	// -----------------------------------------------------------------

	// Create endpoint GET: /api/pendingconfessions/:pendingconfession_id
	api_router.get('/pendingconfessions/:pendingconfession_id', function(req, res) {

		// Extract confession id from url
		var id = req.params.pendingconfession_id;

		pendingConfessionController.read_resource(id, function(err, data){

			if (err) {
				console.log('Error GET: api/pendingconfessions/:pendingconfession_id');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 404) {
					// Send not found error
					// The consumer referenced an inexistant resource or collection.
					res.status(404).json({ error: 'Inexistant resource or collection requested. :('});
				}
			} else {
				// data will contain pending confession object
				// 200 OK: The consumer requested data from the server, and the server found it for them.
				res.status(200).json(data);
			}

		});
	});



	// Create endpoint PUT: /api/pendingconfessions/:pendingconfession_id
	api_router.put('/pendingconfessions/:pendingconfession_id', function(req, res) {

		// Extract confession id from url
		var id = req.params.pendingconfession_id;

		// Extract PUT variables
		var action = req.body.action;

		pendingConfessionController.update(id, action, function(err, data) {

			if (err) {
				console.log('Error PUT: api/pendingconfessions/:pendingconfession_id');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 404) {
					// Send not found error
					// The consumer referenced an inexistant resource or collection.
					res.status(404).json({ error: 'Inexistant resource or collection requested. :('});
				} else if (err == 400) {
					// Send invalid request
					// The consumer gave bad data to the server, and the server did nothing with it.
					res.status(400).json({ error: 'Invalid request. Invalid action parameter. :('});
				}
			} else {
				// data will contain information about the newly created document in pending_confessions collection
				// 201 CREATED: The consumer gave the server data, and the server created a resource.
				res.status(201).json(data);
			}

		});
	});



	// Create endpoint DELETE: /api/pendingconfessions/:pendingconfession_id
	api_router.delete('/pendingconfessions/:pendingconfession_id', function(req, res) {

		var id = req.params.pendingconfession_id;

		pendingConfessionController.remove(id, function(err) {
			if (err) {
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				}
			} else {
				// 204 NO CONTENT: The consumer asked the server to delete a resource, and the server deleted it.
				res.status(204).json();
			}

		});
	});



	// Create endpoint PUT: /api/pendingconfessions/:pendingconfession_id/approve
	api_router.put('/pendingconfessions/:pendingconfession_id/approve', function(req, res) {

		var id = req.params.pendingconfession_id;

		pendingConfessionController.approve(id, function(err, data) {
			if (err) {
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 404) {
					// Send not found error
					// The consumer referenced an inexistant resource or collection.
					res.status(404).json({ error: 'Inexistant resource or collection requested. :('});
				}
			} else {
				// data will contain newly created document in confessions collection
				// 201 CREATED: The consumer gave the server data, and the server created a resource.
				res.status(201).json(data);
			}
		});
	});

}