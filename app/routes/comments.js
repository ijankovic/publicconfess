module.exports = function(api_router){


	// Load internal packages
	var commentController = require('../controllers/comment');



	// ------------------------------------------------------------
	//				/api/confessions/:id/comments
	// ------------------------------------------------------------



	// Create endpoint GET: api/confessions/:confession_id/comments
	api_router.get('/confessions/:confession_id/comments', function(req, res) {

		var reply_to_id = req.params.confession_id;
		var reply_to = "confession";

		// Extract GET variables
		// All parameters are optional
		var limit =  (req.param('limit')) ? req.param('limit') : null;				// default limit null means no limit
		var offset = (req.param('offset')) ? req.param('offset') : 0;				// default offset is 0
		var sortby = (req.param('sortby')) ? req.param('sortby') : "time";			// default sorting is by popular
		var sortorder = (req.param('sortorder')) ? req.param('sortorder') : "desc";	// default sort order is descending

		commentController.read_collection(reply_to_id, reply_to, limit, offset, sortby, sortorder, function(err, data) {

			// check for any errors
			if (err) {
				console.log('Error GET: api/confessions/:confession_id/comments');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 400) {
					// Send invalid request code 400
					// The consumer gave bad data to the server and the server did nothing with it.
					res.status(400).json({ error: 'Invalid parameters. :('});
				}
			} else {
				// data will contain information about the newly created document in comments collection
				// 201 CREATED: The consumer gave the server data, and the server created a resource.
				res.status(200).json(data);
			}
		});
	});


	// Create endpoint POST: api/confessions/:confession_id/comments
	api_router.post('/confessions/:confession_id/comments', function(req, res) {

		var reply_to_id = req.params.confession_id;
		var reply_to = "confession";

		// Extract POST variables
		var content = req.body.content;
		var author = req.body.author;

		commentController.create(content, reply_to_id, reply_to, author, function(err, data) {

			// check for any errors
			if (err) {
				console.log('Error POST: api/confessions/:confession_id/comments');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 400) {
					// Send invalid request code 400
					// The consumer gave bad data to the server and the server did nothing with it.
					res.status(400).json({ error: 'Invalid parameters. Content or author name is inappropriate. Please no bad words. :('});
				}
			} else {
				// data will contain information about the newly created document in pending_confessions collection.
				console.log('New comment posted. { _id: ' + data._id + ', reply_to: "' + data.reply_to +
												'", reply_to_id: ' + data.reply_to_id + ', author: "' + data.author + 
												'", content: "' + data.content + '" }');

				// 201 CREATED: The consumer gave the server data, and the server created a resource.
				res.status(201).json(data);
			}
		});
	});


	// ------------------------------------------------------------
	//					/api/comments/
	// ------------------------------------------------------------



	// TO DO:
	// Create endpoint GET: api/comments/:comment_id/comments
	api_router.get('/comments/:comment_id/comments', function(req, res) {

		var reply_to_id = req.params.comment_id;
		var reply_to = "comment";

		// // Extract GET variables
		// // All parameters are optional
		var limit =  (req.param('limit')) ? req.param('limit') : null;				// default limit null means no limit
		var offset = (req.param('offset')) ? req.param('offset') : 0;				// default offset is 0
		var sortby = (req.param('sortby')) ? req.param('sortby') : "time";			// default sorting is by popular
		var sortorder = (req.param('sortorder')) ? req.param('sortorder') : "desc";	// default sort order is descending

		commentController.read_collection(reply_to_id, reply_to, limit, offset, sortby, sortorder, function(err, data) {

			// check for any errors
			if (err) {
				console.log('Error GET: api/comments/:comment_id/comments');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 400) {
					// Send invalid request code 400
					// The consumer gave bad data to the server and the server did nothing with it.
					res.status(400).json({ error: 'Invalid parameters. :('});
				}
			} else {
				// 200 OK: Server returned data.
				res.status(200).json(data);
			}
		});
	});


	// Create endpoint POST: api/comments/:comment_id/comments
	api_router.post('/comments/:comment_id/comments', function(req, res) {

		var reply_to_id = req.params.comment_id;
		var reply_to = "comment";

		// Extract POST variables
		var content = req.body.content;
		var author = req.body.author;

		commentController.create(content, reply_to_id, reply_to, author, function(err, data) {

			// check for any errors
			if (err) {
				console.log('Error POST: api/comment/:comment_id/comments');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 400) {
					// Send invalid request code 400
					// The consumer gave bad data to the server and the server did nothing with it.
					res.status(400).json({ error: 'Invalid parameters. Content or author name is inappropriate. Please no bad words. :('});
				}
			} else {
				// data will contain information about the newly created document in pending_confessions collection
				console.log('New comment posted. { _id: ' + data._id + ', reply_to: "' + data.reply_to +
												'", reply_to_id: ' + data.reply_to_id + ', author: "' + data.author + 
												'", content: "' + data.content + '" }');

				// 201 CREATED: The consumer gave the server data, and the server created a resource.
				res.status(201).json({ comment_id: data});
			}
		});
	});


	// Create endpoint PUT: /api/comments/:comment_id
	api_router.put('/comments/:comment_id', function(req, res) {

		// Extract comment id from url
		var id = req.params.comment_id;

		// Extract PUT variables
		var action = req.body.action;

		commentController.update(id, action, function(err, data) {

			if (err) {
				console.log('Error PUT: api/comments/:comment_id');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 404) {
					// Send not found error
					// The consumer referenced an inexistant resource or collection.
					res.status(404).json({ error: 'Inexistant resource or collection requested. :('});
				} else if (err == 400) {
					// Send invalid request
					// The consumer gave bad data to the server, and the server did nothing with it.
					res.status(400).json({ error: 'Invalid request. Invalid action parameter. :('});
				}
			} else {
				// On update actions we return no data. Only status code.
				res.status(201).end(); // End response process without any data.
			}
		});
	});


	// Create endpoint DELETE: api/comments/:comment_id
	api_router.delete('/comments/:comment_id', function(req, res) {

		var id = req.params.comment_id;

		commentController.remove(id, function(err) {
			if (err) {
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				}
			} else {
				// 204 NO CONTENT: The consumer asked the server to delete a resource, and the server deleted it.
				res.status(204).json();
			}
		});
	});

}