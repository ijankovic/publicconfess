module.exports = function(public_router, public_dir) {

	// Load internal packages
	var premadeContentController = require('../controllers/premade_content');
	var posterController = require('../poster');

	// Create endpoint GET: /parse_premade_content
	public_router.get('/parse_premade_content', function(req, res) {

		premadeContentController.parsePremadeConfessions();
		console.log("Parsing process started.");
		res.status(200).end();
	});


	// Create endpoint GET: /create_poster
	public_router.get('/create_poster', function(req, res) {

		var content = req.param('text');

		posterController.createSquarePosterForDownload(content, function(err) {
			//res.status(200).send("Poster created successfully!");
			//res.sendFile(public_dir + "/img/temp_square_poster.png");
			if (!err) {
				res.download(public_dir + "/img/temp_square_poster.png");
			}
		})
		// premadeContentController.parsePremadeConfessions();
		// console.log("Parsing process started.");
		// res.status(200).end();
	});

}