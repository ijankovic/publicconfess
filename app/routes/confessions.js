module.exports = function(api_router){



	// Load internal packages
	var utils = require('../utils');
	var confessionController = require('../controllers/confession');



	// ---------------------------------------------------------------
	//					/api/confessions/
	// ---------------------------------------------------------------



	// Create endpoint GET: api/confessions
	api_router.get('/confessions', function(req, res) {

		// Extract GET variables
		// All parameters are optional

		var limit =  (req.param('limit')) ? req.param('limit') : null;				// default limit null means no limit
		var offset = (req.param('offset')) ? req.param('offset') : 0;				// default offset is 0
		var sortby = (req.param('sortby')) ? req.param('sortby') : "time";			// default sorting is by popular
		var sortorder = (req.param('sortorder')) ? req.param('sortorder') : "desc";	// default sort order is descending


		//var from = (req.param('from')) ? req.param('from') : utils.findMonday();	// default from is Monday of the current week
		//var to = (req.param('to')) ? req.param('to') : utils.findSunday();			// default to is Sunday of the current week
		// from and to paramaters must be in the form: 2015-01-04T00:00:00.000Z


		confessionController.read_collection(limit, offset, sortby, sortorder, function(err, data) {

			// check for any errors
			if (err) {
				console.log('Error GET: api/confessions');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 400) {
					// Send invalid request code 400
					// The consumer gave bad data to the server and the server did nothing with it.
					res.status(400).json({ error: 'Invalid parameters. :('});
				}
			} else {
				// data will contain information about the newly created document in confessions collection
				// 201 CREATED: The consumer gave the server data, and the server created a resource.
				res.status(200).json(data);
			}
		});
	});



	// Create endpoint GET: api/confessions/:confession_id
	api_router.get('/confessions/:confession_id', function(req, res) {

		// Extract confession id from url
		var id = req.params.confession_id;

		confessionController.read_resource(id, function(err, data){

			if (err) {
				console.log('Error GET: api/confessions/:pendingconfession_id');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 404) {
					// Send not found error
					// The consumer referenced an inexistant resource or collection.
					res.status(404).json({ error: 'Inexistant resource or collection requested. :('});
				}
			} else {
				// data will contain pending confession object
				// 200 OK: The consumer requested data from the server, and the server found it for them.
				res.status(200).json(data);
			}

		});
	});



	// Create endpoint PUT: api/confessions/:confession_id
	api_router.put('/confessions/:confession_id', function(req, res) {

		// Extract confession id from url
		var id = req.params.confession_id;

		// Extract PUT variables
		var action = req.body.action;

		confessionController.update(id, action, function(err, data) {

			if (err) {
				console.log('Error PUT: api/confessions/:confession_id');
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				} else if (err == 404) {
					// Send not found error
					// The consumer referenced an inexistant resource or collection.
					res.status(404).json({ error: 'Inexistant resource or collection requested. :('});
				} else if (err == 400) {
					// Send invalid request
					// The consumer gave bad data to the server, and the server did nothing with it.
					res.status(400).json({ error: 'Invalid request. Invalid action parameter. :('});
				}
			} else {
				// On update actions we return no data. Only status code.
				res.status(201).end(); // End response process without any data.
			}

		});
	});



	// Create endpoint DELETE: api/confessions/:confession_id
	api_router.delete('/confessions/:confession_id', function(req, res) {

		var id = req.params.confession_id;

		confessionController.remove(id, function(err) {
			if (err) {
				if (err == 500) {
					// Send internal server error
					res.status(500).json({ error: 'Something went wrong. :('});
				}
			} else {
				// 204 NO CONTENT: The consumer asked the server to delete a resource, and the server deleted it.
				res.status(204).json();
			}
		});
	});



}