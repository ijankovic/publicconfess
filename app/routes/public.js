// Load external packages
var path = require('path');
var mustache = require('mustache');
var fs = require('fs');

// Load internal packages
var pages = require('../controllers/public');


module.exports = function(public_router, public_dir) {

	// Load HTML templates from files.

	// This will get executed automatically when node server.js is called.
	var new_template = fs.readFileSync(path.join(public_dir, 'new.html'), "utf8");
	var popular_template = fs.readFileSync(path.join(public_dir, 'popular.html'), "utf8");
	var confession_template = fs.readFileSync(path.join(public_dir, 'confession.html'), "utf8");
	var status_template = fs.readFileSync(path.join(public_dir, 'status.html'), "utf8");
	var moderate_template = fs.readFileSync(path.join(public_dir, 'moderate.html'), "utf8");


	// Render functions
	// ===================================================

	function renderPage(page, res, page_number) {

		var page_template;

		if (page == 'new') {
			page_template = new_template;
		} else if (page == 'popular') {
			page_template = popular_template;
		}

		pages.confessionsPage(page, page_number, function(err, view) {

			if (err) {
				// Send internal server error
				res.status(500).send('Something went wrong. :(');
			} else {

				var html = mustache.render(page_template, view);
				res.status(200).send(html);
			}
		});
	}

	function renderModeratePage(res) {
		res.status(200).send(moderate_template);
	}

	function renderSingleConfessionPage(confession_id, res) {

		pages.singleConfessionPage(confession_id, function(err, view) {

			if (err) {
				// Send internal server error
				res.status(500).send('Something went wrong. :(');
			} else {

				if (view.status) {
					var html = mustache.render(status_template, view);
					res.status(200).send(html);
				} else {
					var html = mustache.render(confession_template, view);
					res.status(200).send(html);
				}

			}
		});
	}



	// ROUTES
	// ===================================================

	public_router.get('/', function(req, res) {
		// Display first page by default.
		renderPage('new', res, 1);
	});


	public_router.get('/new', function(req, res) {
		// Display first page by default.
		renderPage('new', res, 1);
	});


	public_router.get('/new/:page', function(req, res) {

		// Extract page number from url
		var page_number = parseInt(req.params.page); // parseInt() converts string to int

		renderPage('new', res, page_number);
	});


	public_router.get('/popular', function(req, res) {
		renderPage('popular', res, 1);
	});


	public_router.get('/popular/:page', function(req, res) {

		// Extract page number from url
		var page_number = parseInt(req.params.page); // parseInt() converts string to int

		renderPage('popular', res, page_number);
	});


	public_router.get('/moderate', function(req, res) {
		//renderPopularPage(res, 1);

		renderModeratePage(res);
	});


	public_router.get('/confessions/:id', function(req, res) {
		var confession_id = parseInt(req.params.id);

		renderSingleConfessionPage(confession_id, res);
	});


	public_router.get('/about', function(req, res) {
		res.sendFile(path.join(public_dir, 'about.html'));
	});


	public_router.get('/rules', function(req, res) {
		res.sendFile(path.join(public_dir, 'rules.html'));
	});


	public_router.get('/faq', function(req, res) {
		res.sendFile(path.join(public_dir, 'faq.html'));
	});


	public_router.get('/contact', function(req, res) {
		res.sendFile(path.join(public_dir, 'contact.html'));
	});


	public_router.get('/feedback', function(req, res) {
		res.sendFile(path.join(public_dir, 'feedback.html'));
	});


	public_router.get('/report-content', function(req, res) {
		res.sendFile(path.join(public_dir, 'report-content.html'));
	});


	public_router.get('/report-bug', function(req, res) {
		res.sendFile(path.join(public_dir, 'report-bug.html'));
	});


	public_router.get('/terms', function(req, res) {
		res.sendFile(path.join(public_dir, 'terms-of-use.html'));
	});


	public_router.get('/privacy', function(req, res) {
		res.sendFile(path.join(public_dir, 'privacy-policy.html'));
	});


	public_router.get('/confession-approval', function(req, res) {
		res.sendFile(path.join(public_dir, 'confession-approval.html'));
	});


	public_router.get('/tips-for-administering', function(req, res) {
		res.sendFile(path.join(public_dir, 'tips-for-administering.html'));
	});


	public_router.get('/loaderio-45ba3dcad3e70b69dcd1201c2178bd8d', function(req, res) {
		res.sendFile(path.join(public_dir, 'loaderio-45ba3dcad3e70b69dcd1201c2178bd8d.txt'));
	});


	public_router.get('/create-poster', function(req, res) {
		res.sendFile(path.join(public_dir, 'create-poster.html'));
	});
	
}