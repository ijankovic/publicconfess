module.exports = function(api_router){

	// Load internal packages
	var sendEmailController = require('../controllers/send_email');

	// Create endpoint POST: /api/contact/send
	api_router.post('/contact/send', function(req, res) {

		// Extract POST variables
		var fromName = req.body.senderName;
		var fromEmail = req.body.senderEmail;
		var messageBody = req.body.messageBody;

		sendEmailController.sendEmail(fromName, fromEmail, messageBody, "Contact Us", function(err) {
			if (err) {
				res.status(500).end();
			} else {
				res.status(201).end();
			}
		});
	});


	// Create endpoint POST: /api/feedback/send
	api_router.post('/feedback/send', function(req, res) {

		// Extract POST variables
		var fromName = req.body.senderName;
		var fromEmail = req.body.senderEmail;
		var messageBody = req.body.messageBody;

		sendEmailController.sendEmail(fromName, fromEmail, messageBody, "Feedback", function(err) {
			if (err) {
				res.status(500).end();
			} else {
				res.status(201).end();
			}
		});
	});


	// Create endpoint POST: /api/report-content/send
	api_router.post('/report-content/send', function(req, res) {

		// Extract POST variables
		var fromName = req.body.senderName;
		var fromEmail = req.body.senderEmail;
		var messageBody = req.body.messageBody;

		sendEmailController.sendEmail(fromName, fromEmail, messageBody, "Report Content", function(err) {
			if (err) {
				res.status(500).end();
			} else {
				res.status(201).end();
			}
		});
	});


	// Create endpoint POST: /api/report-bug/send
	api_router.post('/report-bug/send', function(req, res) {

		// Extract POST variables
		var fromName = req.body.senderName;
		var fromEmail = req.body.senderEmail;
		var messageBody = req.body.messageBody;

		sendEmailController.sendEmail(fromName, fromEmail, messageBody, "Report Bug", function(err) {
			if (err) {
				res.status(500).end();
			} else {
				res.status(201).end();
			}
		});
	});

}