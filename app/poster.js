// poster.js

// IMPORT PACKAGES
// ===================================================

// Load external packages
var gm = require('gm').subClass({imageMagick: true});
var path = require('path');
var fs = require('fs');
var Dropbox = require("dropbox");

// Load internal packages
var utils = require('./utils');



// GLOBAL VARIABLES
// ===================================================

recentColorSchemes = [];
colorSchemes = [
					{ 'background': 'BEIGE','text': 'WHITE'},
					{ 'background': 'BROWN', 'text': 'WHITE'},
					{ 'background': 'CAMARONE', 'text': 'WHITE'},
					{ 'background': 'COLUMBIA_BLUE', 'text': 'WHITE'},
					{ 'background': 'DARK_GREEN', 'text': 'WHITE'},
					{ 'background': 'DENIM', 'text': 'WHITE'},
					{ 'background': 'GOLD', 'text': 'WHITE'},
					{ 'background': 'GRAY', 'text': 'WHITE'},
					{ 'background': 'GREEN', 'text': 'WHITE'},
					{ 'background': 'LILAC', 'text': 'BLACK'},
					{ 'background': 'LIME', 'text': 'BLACK'},
					{ 'background': 'MAROON', 'text': 'WHITE'},
					{ 'background': 'NAVY_BLUE', 'text': 'WHITE'},
					{ 'background': 'NIAGARA', 'text': 'WHITE'},
					{ 'background': 'OLIVE', 'text': 'WHITE'},
					{ 'background': 'ORANGE', 'text': 'WHITE'},
					{ 'background': 'PICTON_BLUE', 'text': 'WHITE'},
					{ 'background': 'PINK', 'text': 'WHITE'},
					{ 'background': 'PURPLE', 'text': 'WHITE'},
					{ 'background': 'PURPLE_HEART', 'text': 'WHITE'},
					{ 'background': 'PURPLE_INDIGO', 'text': 'WHITE'},
					{ 'background': 'RED', 'text': 'WHITE'},
					{ 'background': 'RED_ORANGE', 'text': 'WHITE'},
					{ 'background': 'ROSE', 'text': 'WHITE'},
					{ 'background': 'ROYAL_BLUE', 'text': 'WHITE'},
					{ 'background': 'SCARLET', 'text': 'WHITE'},
					{ 'background': 'SUNGLOW', 'text': 'BLACK'},
					{ 'background': 'TANGERINE', 'text': 'WHITE'},
					{ 'background': 'VANILLA', 'text': 'BLACK'},
					{ 'background': 'WHITE', 'text': 'BLACK'}
				];



// HELPER FUNCTIONS
// ===================================================

function getTextSize(content, base_size) {

	var x = content.length;
	var text_size = 0;

	switch (true) {
		case (x <= 50):
			text_size = base_size + 16;
			break;
		case (x > 50 && x <= 100):
			text_size = base_size + 14;
			break;
		case (x > 100 && x <= 150):
			text_size = base_size + 12;
			break;
		case (x > 150 && x <= 200):
			text_size = base_size + 10;
			break;
		case (x > 200 && x <= 250):
			text_size = base_size + 8;
			break;
		case (x > 250 && x <= 300):
			text_size = base_size + 6;
			break;
		case (x > 300 && x <= 350):
			text_size = base_size + 4;
			break;
		case (x > 350 && x <= 400):
			text_size = base_size + 2;
			break;
		case (x > 400 && x <= 450):
			text_size = base_size;
			break;
	}

	return text_size;
}


function trimContent(content) {

	content = content.substring(0, 444);
	last_space = content.lastIndexOf(" ");
	content = content.substring(0, last_space);
	content += "..."

	return content;
}


// Generate array of valid color schemes.
function getColorSchemesToChooseFrom() {

	// Create array with integers from 0 to colorSchemes.length - 1
	var colorSchemesToChooseFrom = [];
	var lowEnd = 0;
	var highEnd = colorSchemes.length;

	while(lowEnd < highEnd){
		colorSchemesToChooseFrom.push(lowEnd++);
	}

	console.log("Recently used color schemes: " + recentColorSchemes);

	// Remove integers that correspond to recently used colors.
	for (var i = 0; i < recentColorSchemes.length; i++) {

		// Find the index of the recentColor.
		var index = colorSchemesToChooseFrom.indexOf(recentColorSchemes[i]);

		// Remove it.
		if (index > -1) {
			colorSchemesToChooseFrom.splice(index, 1);
		}
	}

	return colorSchemesToChooseFrom;
}


function generateColorScheme() {

	var colorSchemesToChooseFrom = getColorSchemesToChooseFrom();

	var randomIndex = utils.getRandomInt(0, colorSchemesToChooseFrom.length - 1);
	var randomColorSchemeNumber = colorSchemesToChooseFrom[randomIndex];

	// Add the new color to recentColorSchemes.
	recentColorSchemes.push(randomColorSchemeNumber);

	// If there are more than 4 recent colors
	if (recentColorSchemes.length > 4) {
		// Pop the oldest one.
		recentColorSchemes.shift();
	}

	console.log("Poster color scheme: [" + randomIndex + "] " + JSON.stringify(colorSchemes[randomColorSchemeNumber]));

	return colorSchemes[randomColorSchemeNumber];
}


function uploadSquarePosterToDropbox() {

	console.log("Starting image upload to Dropbox.");

	var dropboxClient = new Dropbox.Client({
		key: "a3jpqo050pnudlx",
		secret: "3d2bfov3cvnkky5",
		token:"Xy3MSvNjU_QAAAAAAAAAE9yhup2f3FGn5uIlYrKqRaB9I5jysYQSrgtjHdmCZ92t",
	});

	dropboxClient.authenticate(function(error, client) {

		if (error) {
			console.log("Something happened trying to authenticate with dropbox.");
			console.log(error);
			return;
		}

		fs.readFile("public/img/square_poster.png", function(error3, data) {
			if (error3) {
				console.log("Error in uploadSquarePosterToDropbox: Could not read the image file. " + error3);
			} else {
				var now = new Date();
				var timestamp = now.toISOString();

				client.writeFile("instagram_" + timestamp + ".png", data, function(error4, stat) {
				if (error4) {
					console.log("Error in uploadSquarePosterToDropbox: Could not upload the image to Dropbox. " + error4);
				}
					console.log("Image has been succesfully uploaded to Dropbox.");
				});
			}
		});
	});
}



// EXPORT METHODS
// ===================================================

var createRectanglePoster = function(content, public_dir, callback) {

	if (content.length > 450) {
		content = trimContent(content);
	}

	var text_size = getTextSize(content, 30);
	var colorScheme = generateColorScheme();

	var poster_template = path.join(__dirname, 'posters', 'POSTER_RECTANGLE_' + colorScheme.background + '.png');
	//var output = path.join(__dirname, 'posters', 'rectangle_poster.png');

	gm()
	.command('convert')
	.in(poster_template)
	.in('-gravity', 'Center')
	.in('-size', '880x')
	.in('-background', 'transparent')
	.in('-fill', colorScheme.text)
	.in('-font', 'Open-Sans-Semibold')
	.in('-pointsize', text_size)
	.in('caption: ' + content)
	.in('-geometry', '+0-46')
	.out('-composite')
	.write(public_dir + "/img/rectangle_poster.png", function (err) {
		if (err) {
			console.log("Error in poster.createRectanglePoster: " + err);
			callback(err);
		} else {
			console.log("Poster has been created successfully.");
			callback(null);
		}
	});
}


var createSquarePosterDropbox = function(content) {

	if (content.length > 450) {
		content = trimContent(content);
	}

	var text_size = getTextSize(content, 44);
	var colorScheme = generateColorScheme();
	var poster_template = path.join(__dirname, 'posters', 'POSTER_SQUARE_' + colorScheme.background + '.png');

	gm()
	.command("convert")
	.in(poster_template)
	.in("-gravity", "Center")
	.in("-size", "880x")
	.in("-background", "transparent")
	.in("-fill", colorScheme.text)
	.in("-font", "Open-Sans-Semibold")
	.in("-pointsize", text_size)
	.in('caption:' + content)
	.in("-geometry", "+0-72")
	.out("-composite")
	.write("public/img/square_poster.png", function (err) {
		if (err) {
			console.log("Error in poster.createSquarePoster: " + err);
		} else {
			console.log("Poster has been created successfully.");
			uploadSquarePosterToDropbox();
		}
	});
}


var createSquarePosterForDownload = function(content, callback) {

	if (content.length > 450) {
		content = trimContent(content);
	}

	var text_size = getTextSize(content, 44);
	var colorScheme = generateColorScheme();
	var poster_template = path.join(__dirname, 'posters', 'POSTER_SQUARE_' + colorScheme.background + '.png');

	gm()
	.command("convert")
	.in(poster_template)
	.in("-gravity", "Center")
	.in("-size", "880x")
	.in("-background", "transparent")
	.in("-fill", colorScheme.text)
	.in("-font", "Open-Sans-Semibold")
	.in("-pointsize", text_size)
	.in('caption:' + content)
	.in("-geometry", "+0-72")
	.out("-composite")
	.resize(640, 640)
	.write("public/img/temp_square_poster.png", function (err) {
		if (err) {
			console.log("Error in poster.createSquarePosterForDownload: " + err);
			callback(err);
		} else {
			console.log("Poster has been created successfully.");
			callback(null);
		}
	});

}



exports.createRectanglePoster = createRectanglePoster;
exports.createSquarePosterDropbox = createSquarePosterDropbox;
exports.createSquarePosterForDownload = createSquarePosterForDownload;