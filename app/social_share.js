// social_share.js

// IMPORT PACKAGES
// ===================================================

// Load external packages
var gm = require('gm').subClass({imageMagick: true});
var path = require('path');
var FB = require('fb');
// var Twitter = require('twitter');
var async = require('async');

// Load internal packages
var utils = require('./utils');



// GLOBAL VARIABLES
// ===================================================

// Set authentication for Facebook API.
FB.setAccessToken('CAATr8EJgUYQBACAEwbVBKZBcq2e4mxeidix9DBKs8Nk17FsAVaPC0ak1YKnLyJXFZB9GGl0ZBXyaZA75oNZBZA3CP9fUz11DCuIv3YbewqZBRzI5fITJecI1ZA3ZCpi0TzscWvlk6pbRi0BHqZAj57uLZBVtbPRU0rUlTro3MJ7C5f9uOpYUgWp0lX19XjG2mZAu8DZCsFCZBXK8RCVu9dRy4ybwyj');

// Set authentication for Twitter API.
// var twitterClient = new Twitter({
// 		consumer_key: 'l11mJb7J4wyXwFO1MfUAfyBRQ',
// 		consumer_secret: 's06VS5KsWwwg6nokiE3p9TjmieKfiQRVO5LZ8xVmysO9MYd57T',
// 		access_token_key: '2886457971-pVY61S4Z1LlvWSDPcG7T5qlTm2RR0pfs1zbo3bX',
// 		access_token_secret: 'wOdkjPp5MRehoOgyzi8PivPb0JhFJ4bVfE7gIqd8mtLVW'
// });

// List containing random statuses.
var statuses = [
					"Read more confessions at publicconfess.com/popular",
					"Follow new confessions at publicconfess.com/new",
					"Be Admin at publicconfess.com/moderate"
				];

// Variables specifying what percentage of Facebook posts should contain TEXT + LOGO versus POSTER.
var text_logo_share_percentage = 25;
var poster_share_percentage = 75;



// HELPER FUNCTIONS
// ===================================================

// Check if str contains punctuation
function isPunctuation(str) {

	if (str.match(/[\.,-\/#!\^&\*;?!:{}=\-_`~()"']/g) != null) {
		return true;
	} else {
		return false;
	}
}


// Remove punctuation from the end of str
function stripEndingPunctuation(str) {

	var lastChar = str.substr(str.length - 1);
	var secondLastChar = str.substring(str.length - 2, str.length - 1);
	var thirdLastChar = str.substring(str.length - 3, str.length - 2);

	// Check if last three characters are punctuation.
	if (isPunctuation(lastChar) && isPunctuation(secondLastChar) && isPunctuation(thirdLastChar)) {

		console.log("Three last characters are punctuation.");

		// Strip the last three characters.
		str = str.substring(0, str.length - 3);

	} else if (isPunctuation(lastChar) && isPunctuation(secondLastChar)) {

		console.log("Two last characters are punctuation.");

		// Strip the last two characters.
		str = str.substring(0, str.length - 2);

	} else if (isPunctuation(lastChar)) {

		console.log("One last characters are punctuation.");

		// Strip the last two characters.
		str = str.substring(0, str.length - 1);

	}

	return str;
}


// Get trimmed Facebook link title
function getLinkTitle(content) {

	// Trim if content is more than 50 characters.
	// This is to ensure that link title uses only one line when posted.
	if (content.length > 50) {
		content = content.substring(0, 50);
		var last_space = content.lastIndexOf(" ");
		content = content.substring(0, last_space);
		content = stripEndingPunctuation(content);
		content += "...";
	}

	return content;
}


// Get random status from pre-generated statuses
function getRandomStatus() {

	var randomStatusIndex = utils.getRandomInt(0, statuses.length - 1);
	return statuses[randomStatusIndex];
}


// Get trimmed tweet content
function trimTweetContent(content) {

	// Trim if content is more than 90 characters.
	// If confession has less than 90 characters, output whole confession.
	// 90 characters for confession + 22 characters for url + 22 characters for the image < 140 characters with is the limit.
	if (content.length > 90) {
		content = content.substring(0, 90);
		var last_space = content.lastIndexOf(" ");
		content = content.substring(0, last_space);
		content = stripEndingPunctuation(content);
		content += "...";
	}

	return content;
}


// Get Tweet content
function getRandomTweet(confession) {

	var tweet_content;
	var randomNumber = utils.getRandomInt(1, 100);

	// 50% of the time we are going to tweet confession content + link to confession.
	// Other 50% we will tweet random status linking to popular, new or moderate pages.
	if (randomNumber <= 50) {
		tweet_content = trimTweetContent(confession.content) + " http://publicconfess.com/confessions/" + confession._id;
	} else {
		tweet_content =  getRandomStatus()
	}

	return tweet_content;
}


// Share POSTER + LINK on Facebook
function sharePosterLinkOnFacebook(confession) {

	var status = getRandomStatus();
	var title = getLinkTitle(confession.content);
	var share_url = 'publicconfess.com/confessions/' + confession._id;
	var image_url = 'http://publicconfess.herokuapp.com/img/rectangle_poster.png';

	console.log("TITLE: " + title);
	console.log("STATUS: " + status);

	async.waterfall([
		function(callback) {

			// Upload unpublished photo

			FB.api(
				'401365640014709/photos',
				'POST',
				{
					url: image_url,
					published: false
				},
				function (res) {

					// Check for errors
					if(!res || res.error) {
						console.log("Error trying to post image to Facebook: ");
						console.log(!res ? 'No response.' : res.error);
						callback(res.error, null);
						return;
					}

					// Photo has been uploaded successfully. Pass photo id to next function.
					console.log("Photo uploaded. Id: " + res.id);
					callback(null, res.id);
				}
			);
		},

		function(photo_id, callback) {

			// Get source URL of the uploaded photo.

			FB.api(
				'/' + photo_id,
				'GET',
				{
					fields: ['source'] 
				},
				function (res) {

					// Check for errors
					if (!res && res.error) {
						console.log("Error trying to get image source url from Facebook: ");
						console.log(!res ? 'No response.' : res.error);
						callback(res.error, null);
						return;
					}

					// Photo source url has been retrived successfully. Pass the photo source url to next function.
					console.log("Photo (id=" + photo_id + ") source url: " + res.source);
					callback(null, res.source);
				}
			);
		},

		function(photo_source_url, callback) {

			// Post link on the page.

			FB.api(
				'401365640014709/feed',
				'POST',
				{
					message: status,
					link: share_url,
					picture: photo_source_url,
					name: title,
					description: confession.content,
					caption: 'publicconfess.com',
				},
				function (res) {

					// Check for errors
					if(!res || res.error) {
						console.log("Error trying to post link to Facebook: ");
						console.log(!res ? 'No response.' : res.error);
						return;
					}

					// Link has been posted successfully. Pass the photo source url to next function.
					console.log("Facebook link shared successfully. Post Id: " + res.id);
					callback(null, null);	// no errors and no result
				}
			);
		}],

		function (err, result) {
			
			if (err) {
				console.log("Error in shareLinkOnFacebook.");
			} else {
				console.log("shareLinkOnFacebook has been completed successfully.");
			}
		}
	);
}


// Share LINK + TEXT + LOGO on Facebook.
function shareTextLogoLinkOnFacebook(confession) {

	var status = confession.content + "\n\nRead comments at: publicconfess.com/confessions/" + confession._id;
	var title = getLinkTitle(confession.content);
	var share_url = 'publicconfess.com/confessions/' + confession._id;
	var image_url = 'http://publicconfess.herokuapp.com/img/facebook_share_banner.png';

	console.log("Link Title: " + title);
	console.log("Status: " + status);


	FB.api(
		'401365640014709/feed',
		'POST',
		{
			message: status,
			link: share_url,
			picture: image_url,
			name: title,
			description: confession.content,
			caption: 'publicconfess.com',
		},
		function (res) {

			// Check for errors
			if(!res || res.error) {
				console.log("Error trying to post link to Facebook: ");
				console.log(!res ? 'No response.' : res.error);
				return;
			}

			// Link has been posted successfully. Pass the photo source url to next function.
			console.log("Link shared successfully. Post Id: " + res.id);
		}
	);
}



// EXPORT METHODS
// ===================================================

var shareLinkOnFacebook = function(confession) {

	// Decide whether to post "text + logo" link or "poster" link.

	var randomNumber = utils.getRandomInt(1, 100);

	if (randomNumber <= text_logo_share_percentage) {
		console.log("Sharing TEXT LOGO link on Facebook.");
		shareTextLogoLinkOnFacebook(confession);
	} else {
		console.log("Sharing POSTER link on Facebook.");
		sharePosterLinkOnFacebook(confession);
	}
}


var shareOnTwitter = function(public_dir, confession) {

	console.log("Sharing on Twitter.");

	// Load image
	var image = require('fs').readFileSync(path.join(public_dir, 'img', 'rectangle_poster.png'));

	// Upload image to Twitter
	// twitterClient.post('media/upload', {media: image},  function(error, media, response){
		
	// 	if (!error) {

	// 		var tweet_content = getRandomTweet(confession);
	// 		console.log("Tweet content: " + tweet_content);

	// 		// Lets tweet it
	// 		var tweet_object = {
	// 			status: tweet_content,
	// 			media_ids: media.media_id_string // Pass the media id string
	// 		}

	// 		// Post tweet
	// 		twitterClient.post('statuses/update', tweet_object, function(error, tweet, response){
	// 			if (!error) {
	// 				console.log("Tweet shared successfully. Tweet Id: " + tweet.id_str);
	// 				console.log("shareLinkOnTwitter has been completed successfully.");
	// 			} else {
	// 				console.log("Error in shareOnTwitter: " + JSON.stringify(error));
	// 			}
	// 		});

	// 	} else {
	// 		console.log("Error in shareOnTwitter: " + JSON.stringify(error));
	// 	}
	// });
}



exports.shareLinkOnFacebook = shareLinkOnFacebook;
exports.shareOnTwitter = shareOnTwitter;