// utils.js

// IMPORT PACKAGES
// ===================================================

// Load models
var Counter = require('./models/counter');
var TaskTracker = require('./models/task_tracker');



// HELPER FUNCTIONS
// ===================================================

// Check if string contains any characters that could be used in malicious code insertion
function checkForAttack(input) {

	var attack = true; 

	// Search input string for characters that could be used in malicious code insertion.
	if (input.search("=") == -1 && 	input.search("<") == -1 && input.search(">") == -1 ) {
		attack = false;	// input is NOT SAFE
	}

	return attack;
}


function checkForEmailOrURL(input) {
	var regex = new RegExp("(http(s)?://.)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)");
	return regex.test(input);
}


// function checkIfUnacceptable(input) {

// 	var unacceptableWords = ['asshole'];
// 	var regexPattern = "(\\ahole\\b|\\banal\\b)";
// 	var regex = new RegExp(regexPattern);

// 	console.log(regex.test(input));

// 	if (regex.test(input)) {
// 		console.log("Content is unacceptable.");
// 	} else {
// 		console.log("Content is ok.");
// 	}
// }


function checkIfInappropriate(input) {

	var regexPattern = "(\\banus\\w*\\b|\\bass\\w*\\b|\\bazz\\w*\\b|\\bbang\\w*\\b|\\bbastard\\w*\\b|\\bbiatch\\w*\\b|\\bbitch\\w*\\b|\\bblow\\w*\\b|\\bboob\\w*\\b|\\bbreast\\w*\\b|\\bbutt\\w*\\b|\\bcawk\\w*\\b|\\bclit\\w*\\b|\\bcock\\w*\\b|\\bcrap\\w*\\b|\\bcum\\w*\\b|\\bcunt\\w*\\b|\\bdick\\w*\\b|\\bdildo\\w*\\b|\\bdominatri\\w*\\b|\\bejaculat\\w*\\b|\\bf u c k\\w*\\b|\\bfag\\w*\\b|\\bfece\\w*\\b|\\bforeskin\\w*\\b|\\bfuck\\w*\\b|\\bfuk\\w*\\b|\\bgay\\w*\\b|\\bjackoff\\w*\\b|\\bjerk\\w*\\b|\\bjizz\\w*\\b|\\blesbian\\w*\\b|\\blick\\w*\\b|\\bmasochist\\w*\\b|\\bmasterbat\\w*\\b|\\bmasturbat\\w*\\b|\\bmotherfuck\\w*\\b|\\bn1gr\\w*\\b|\\bnigga\\w*\\b|\\bnigger\\w*\\b|\\bnigur\\w*\\b|\\bniiger\\w*\\b|\\bniigr\\w*\\b|\\borgasm\\w*\\b|\\bpenis\\w*\\b|\\bphuck\\w*\\b|\\bphuk\\w*\\b|\\bpoop\\w*\\b|\\bporn\\w*\\b|\\bpusse\\w*\\b|\\bpussy\\w*\\b|\\brectum\\w*\\b|\\bretard\\w*\\b|\\bsadist\\w*\\b|\\bscank\\w*\\b|\\bscrotum\\w*\\b|\\bsemen\\w*\\b|\\bsex\\w*\\b|\\bsh t\\w*\\b|\\bshit\\w*\\b|\\bskank\\w*\\b|\\bslut\\w*\\b|\\bsperm\\w*\\b|\\bsuck\\w*\\b|\\btit\\w*\\b|\\bturd\\w*\\b|\\btwat\\w*\\b|\\bvagina\\w*\\b|\\bvulva\\w*\\b|\\bwhore\\w*\\b|\\bxxx\\w*\\b)";
	var regex = new RegExp(regexPattern);

	return regex.test(input);
}


// EXPORT FUNCTIONS
// ===================================================

// getNextId implements Counter Collection.
// Use a separate counters collection to track the last number sequence used.
// The _id field contains the sequence name and the seq field contains the last value of the sequence.

// This is used to determine id of new confession or comment. Something like auto-increment.

var getNextId = function(name, callback) {

	var query = {"_id": name};
	var update = {$inc: { seq: 1 }};
	var options = {new: true, upsert: true};
	Counter.findOneAndUpdate(query, update, options, function(err, data) {
		if (err) {
			console.log("Error in getNextId");
		}

		// data is updated document in 'counters' collections
		// return updated seq field from 'counters' collections
		callback(data.seq);
	});
}


// increaseCount function is exactly the same as getNextId. It is used to increase count of popular confessions and published confessions.
// To avoid confusion we decided to have another name fro getNextId function.
var increaseCount = getNextId;


// Returns a random integer between min (inclusive) and max (inclusive)
var getRandomInt = function (min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


// Checks whether content submitted is ok or not.
var checkContent = function(content) {

	var content = content.toLowerCase();

	if (checkForEmailOrURL(content) || checkIfInappropriate(content)) {

		// Return true if problems have been found.
		return true;
	}

	// Return false if no problems have been found.
	return false;
}



// TASK TRACKER
// ===================================================

// Update task log with the timestamp of the latest job execution.
var updateLatestTime = function(task_name, latest_timestamp, callback) {

	TaskTracker.findByIdAndUpdate(task_name, { timestamp: latest_timestamp }, function(err) {
		if (err) {
			console.log("Error trying to update task tracker latest time in TaskTracker.updateLatestTime: " + err);
		}

		callback();
	});
}


// Return a timestamp of the latest time the task was performed.
var getLatestTime = function(task_name, callback) {

	TaskTracker.findById(task_name, function (err, result) {

		if (err || result == null) {
			callback(true, result);		// Send error.
		} else {
			callback(null, result.timestamp);	
		}
	});
}



exports.getNextId = getNextId;
exports.increaseCount = increaseCount;
exports.checkForAttack = checkForAttack;
exports.checkContent = checkContent;
exports.getRandomInt = getRandomInt;
exports.getLatestTime = getLatestTime;
exports.updateLatestTime = updateLatestTime;